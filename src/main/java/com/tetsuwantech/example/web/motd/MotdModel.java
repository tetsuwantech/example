package com.tetsuwantech.example.web.motd;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.Instant;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.tetsuwantech.atom.util.DateTimePeriod;
import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.example.database.motd.MotdEntity;

public class MotdModel extends GenericModel implements DateTimePeriod {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String title;

	@NotNull
	@Size(min = 1)
	private String description;

	@NotNull
	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant startDate;

	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant endDate;

	private Boolean resetAllShowMotd = false;

	public MotdModel() {
	}

	public MotdModel(MotdEntity motdEntity) {
		super(motdEntity);
		setTitle(motdEntity.getTitle());
		setDescription(motdEntity.getDescription());
		setStartDate(motdEntity.getStartDate());
		setEndDate(motdEntity.getEndDate());
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	@Override
	public Instant getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	@Override
	public Instant getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

	public Boolean getResetAllShowMotd() {
		return resetAllShowMotd;
	}

	public void setResetAllShowMotd(Boolean resetAllShowMotd) {
		this.resetAllShowMotd = resetAllShowMotd;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof MotdModel)) {
			return false;
		}

		return true;
	}

}
