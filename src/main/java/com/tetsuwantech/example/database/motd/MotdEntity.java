package com.tetsuwantech.example.database.motd;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.Instant;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;
import com.tetsuwantech.atom.util.DateTimePeriod;

@Entity
@Cacheable
@Table(name = "motd")
public class MotdEntity extends GenericEntity implements DateTimePeriod {

	private static final long serialVersionUID = 1L;

	@Auditable
	@Column(length = LONG_LENGTH)
	@NotNull
	@Size(min = 1, max = LONG_LENGTH)
	private String title;

	@Auditable
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	@Auditable
	@Column(name = "start_date")
	@NotNull
	private Instant startDate;

	@Auditable
	@Column(name = "end_date")
	private Instant endDate;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	@Override
	public Instant getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	@Override
	public Instant getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

}
