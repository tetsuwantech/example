package com.tetsuwantech.example.manager.notebook.note;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.config.ConfigService;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.database.notebook.NotebookService;
import com.tetsuwantech.example.database.notebook.note.Note;
import com.tetsuwantech.example.database.notebook.note.NoteService;
import com.tetsuwantech.example.web.Sequenceable;
import com.tetsuwantech.example.web.note.NoteModel;
import com.tetsuwantech.example.web.notebook.NotebookModel;

@Named(value = "NoteManager")
public class NoteManager extends GenericManager<Note, Notebook, NoteModel, NotebookModel> {

	@Inject
	private SubjectService subjectService;

	@Inject
	private ConfigService configService;

	@Inject
	private NotebookService notebookService;

	@Inject
	private NoteService noteService;

	@Override
	public NoteModel createModel(Note note) {
		return new NoteModel(note);
	}

	@Override
	public void save(NoteModel noteModel) {

		// if new, create a note, otherwise populate the new bits
		Note note;
		boolean isNew = false;
		if (noteModel.getId() == null) {

			note = new Note();
			Notebook notebook = notebookService.get(noteModel.getNotebook().getId());
			note.setNotebook(notebook);

			// This works because first note sequence is 0
			note.setSequence((long) notebook.getNotes().size());

			isNew = true;

		} else {
			note = noteService.get(noteModel.getId());
		}

		// set the data that's the same for the two states (new, saved)
		note.setTitle(noteModel.getTitle());
		note.setText(noteModel.getText());

		// set this, as it won't be around
		if (isNew) {

			// make a new one
			noteService.create(note);
			noteModel.setId(note.getId());

			// need to reload this as we've just added a note
		} else {
			// save this as it's straight in
			noteService.save(note);
		}

	}

	@Override
	public List<NoteModel> findAll(NotebookModel notebookModel) {
		Notebook notebook = notebookService.get(notebookModel.getId());
		return super.findAll(notebook, null, "sequence");
	}

	public Long count(NotebookModel notebookModel) {
		Notebook notebook = notebookService.get(notebookModel.getId());
		return super.count(notebook, null);
	}

	@Override
	public Long countAll(SubjectModel subjectModel) {

		Subject subject = subjectService.get(subjectModel.getId());
		Config config = configService.findFirst(subject);
		List<Notebook> notebooks = notebookService.findAll(config);

		// kids, don't do this at home
		Long count = notebooks.stream().map(notebook -> notebookService.findAll(config)).flatMap(List::stream).mapToLong(notebook -> super.count(notebook, null)).sum();
		return count;
	}

	@Override
	public List<NoteModel> search(NotebookModel notebookModel, String text) {
		Notebook notebook = notebookService.get(notebookModel.getId());
		return super.search(notebook, null, text, new String[] { "title", "description" });
	}

	public void updateSequence(NoteModel model) {
		Note entity = noteService.get(model.getId());
		((com.tetsuwantech.example.database.Sequenceable) entity).setSequence(((Sequenceable) model).getSequence());
		noteService.save(entity);
	}

}
