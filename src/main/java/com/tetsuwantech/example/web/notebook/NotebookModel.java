package com.tetsuwantech.example.web.notebook;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;

import com.tetsuwantech.atom.web.DeleteModel;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.web.Sequenceable;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.search.Searchable;

@Scope(value = "session")
public class NotebookModel extends DeleteModel implements Sequenceable, Searchable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String title;

	// sequence does not need @Valid items as this is handled by javascript/ajax
	private Long sequence;
	private String description;

	private ConfigModel config;
	private String searchPath;

	public NotebookModel() {
	}

	public NotebookModel(Long id) {
		super(id);
	}

	public NotebookModel(Long id, String title) {
		super(id);
		setTitle(title);
	}

	public NotebookModel(Notebook notebook) {
		super(notebook);

		setTitle(notebook.getTitle());
		setSequence(notebook.getSequence());
		setDescription(notebook.getDescription());

		ConfigModel configModel = new ConfigModel(notebook.getConfig().getId());
		setConfig(configModel);
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the sequence
	 */
	@Override
	public Long getSequence() {
		return sequence;
	}

	/**
	 * @param sequence
	 *            the sequence to set
	 */
	@Override
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public ConfigModel getConfig() {
		return this.config;
	}

	public void setConfig(ConfigModel config) {
		this.config = config;
	}

	/**
	 * @return the path
	 */
	@Override
	public void setSearchPath(String searchPath) {
		this.searchPath = searchPath;
	}

	@Override
	public String getSearchPath() {
		return searchPath;
	}

	@Override
	public String getSearchURL() {
		return "/notebook/edit?id=" + getId();
	}

	// For lucene highlight
	private String searchDescription;

	@Override
	public void setSearchDescription(String searchDescription) {
		this.searchDescription = searchDescription;
	}

	@Override
	public String getSearchDescription() {
		return searchDescription;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof NotebookModel)) {
			return false;
		}

		return true;
	}
}
