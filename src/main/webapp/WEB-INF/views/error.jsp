<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" isErrorPage="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">error</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">error</c:param>
    </c:import>

    <div id="main" class="container">

        <div class="card border-danger">
            <h4 class="card-header bg-danger text-white text-center">
                <fmt:message key="errors" />
            </h4>
            <div class="card-body">
                <h2>Oops</h2>
                Oops, something went wrong.
                <hr />

                    <c:choose>
                        <c:when test="${pageContext.errorData.statusCode eq 404}">
                            <h4>404: Page Not Found</h4>
${pageContext.request.scheme}://${pageContext.request.serverName}${(pageContext.request.serverPort != 80 && pageContext.request.serverPort != 443) ? ':'.concat(pageContext.request.serverPort) : ''}${pageContext.request.contextPath}${pageContext.errorData.requestURI}<br />
                        </c:when>
                        <c:when test="${pageContext.errorData.statusCode eq 500}">
                            <h4>500: Internal Server Error</h4>
                            <c:out value="${pageContext.exception}" />
                            <!-- 
<c:forEach items="${pageContext.exception.stackTrace}" var="stackTrace">${stackTrace}
</c:forEach>
 -->
                        </c:when>
                        <c:otherwise>
                            <c:out value="${exception}" />
                            <!-- 
<c:forEach items="${exception.stackTrace}" var="stackTrace">${stackTrace}
</c:forEach>
 -->
                        </c:otherwise>
                    </c:choose>
            </div>
        </div>
    </div>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
