package com.tetsuwantech.example.database.notebook;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.lucene.analysis.charfilter.HTMLStripCharFilterFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.CharFilterDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import com.tetsuwantech.atom.database.Deletable;
import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;
import com.tetsuwantech.atom.database.authentication.Securable;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.example.database.Sequenceable;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.notebook.note.Note;

@Entity
@Cacheable
@Indexed(index = "notebook")
@Table(name = "notebook")
@AnalyzerDef(name = "html_analyzer", tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = { @TokenFilterDef(factory = LowerCaseFilterFactory.class), }, charFilters = { @CharFilterDef(factory = HTMLStripCharFilterFactory.class) })
public class Notebook extends GenericEntity implements Securable, Deletable, Sequenceable {

	private static final long serialVersionUID = 1L;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Auditable
	@Column(length = LONG_LENGTH)
	@NotNull
	@Size(min = 1, max = LONG_LENGTH)
	private String title;

	@Auditable
	@NotNull
	@Min(value = 0)
	private Long sequence;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Auditable
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	@IndexedEmbedded(includePaths = { "id" })
	@Auditable
	@ManyToOne(targetEntity = Config.class)
	@JoinColumn(name = "config_id")
	private Config config;

	@OneToMany(targetEntity = Note.class, mappedBy = "notebook")
	List<Note> notes = new LinkedList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public Long getSequence() {
		return sequence;
	}

	@Override
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes.clear();
		this.notes.addAll(notes);
	}

	@Override
	public boolean isDeletable() {
		return notes.isEmpty();
	}

	@Override
	public Subject getSubject() {
		return getConfig().getSubject();
	}

}
