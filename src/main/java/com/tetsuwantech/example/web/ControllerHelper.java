package com.tetsuwantech.example.web;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Iterator;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.springframework.context.MessageSource;

import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.example.NotSequenceableException;
import com.tetsuwantech.example.manager.SequenceManager;
import com.tetsuwantech.example.web.config.ConfigModel;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

// can't make this a controller with mappings
// https://jira.spring.io/browse/SPR-11421
public class ControllerHelper<M extends GenericManager<?, ?, F, ?>, F extends GenericModel, T extends GenericModel> {

	// private static Logger LOG = LoggerFactory.getLogger(ControllerHelper.class);

	private final M manager;
	private final Mapping controllerMapping;
	private final MessageSource messageSource;

	public enum Mapping {
		NOTEBOOK("notebooks"),
		NOTE("notes"),
		MOTD("motds");

		private String target;
		private String path;

		Mapping(String target) {
			this.target = target;
		}

		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}

		/**
		 * @return the edit
		 */
		public String getPath() {
			return this.path;
		}

		/**
		 * @param path
		 *            the edit to set
		 */
		public void setPath(String path) {
			this.path = path;
		}

	}

	public ControllerHelper(M manager, Mapping controllerMapping, MessageSource messageSource) {
		this.manager = manager;
		this.controllerMapping = controllerMapping;
		this.messageSource = messageSource;
	}

	public String remove(ConfigModel config, F model) {
		manager.remove(model.getId());

		String message = messageSource.getMessage(controllerMapping.getTarget() + ".main.delete", null, null);
		JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder().add("message", message);
		JsonObject jsonObject = jsonObjectBuilder.build();
		return jsonObject.toString();
	}

	@SuppressWarnings(value = "unchecked")
	public String reorder(List<F> models) {

		Iterator<F> iterator = models.iterator();
		while (iterator.hasNext()) {
			F model = iterator.next();

			if (!(model instanceof Sequenceable)) {
				throw new NotSequenceableException(model.getClass().toString());
			}

			((SequenceManager<F>) manager).updateSequence(model);
		}

		String message = messageSource.getMessage(controllerMapping.getTarget() + ".main.reorder", null, null);
		JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder().add("message", message);
		JsonObject jsonObject = jsonObjectBuilder.build();
		return jsonObject.toString();
	}

}
