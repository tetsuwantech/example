package com.tetsuwantech.example.web.motd;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

public class MotdModelValidator implements Validator {

	// private static Logger LOG = LoggerFactory.getLogger(MotdModelValidator.class);

	public MotdModelValidator() {
	}

	@Override
	public boolean supports(Class<?> target) {
		return MotdModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {
		MotdModel motd = (MotdModel) object;

		// check for timezones - should be fine if not converted as we just want start
		// before end
		if (motd.getStartDate() != null && motd.getEndDate() != null && motd.getStartDate().isAfter(motd.getEndDate())) {
			errors.rejectValue("startDate", "StartDate.afterEndDate");
		}
	}
}
