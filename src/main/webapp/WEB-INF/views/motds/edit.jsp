<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">motd</c:param>
</c:import>
<c:set var="config" value="${sessionScope.config}" />

<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">admin</c:param>
    </c:import>

    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">edit motd</h4>
            <div class="card-body">

                <c:url var="action" value="/admin/motd/save" />
                <form:form action="${action}" cssClass="form-horizontal" modelAttribute="motd" method="POST">
                    <form:hidden path="id" />

                    <spring:bind path="title">
                        <div class="form-group row">
                            <label for="title" class="col-form-label col-md-3 text-right">title</label>
                            <div class="col-md-9">
                                <fmt:message var="tooltip" key="motd.title.tooltip" />
                                <form:input path="title" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="title" data-toggle="tooltip" title="${tooltip}" />
                                <form:errors path="title" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="description">
                        <div class="form-group row">
                            <label for="description" class="col-form-label col-md-3 text-right">description</label>
                            <div class="col-md-9">
                                <fmt:message var="tooltip" key="motd.description.tooltip" />
                                <form:textarea path="description" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="description" data-toggle="tooltip" title="${tooltip}" />
                                <form:errors path="description" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="startDate">
                        <div class="form-group row">
                            <label for="startDate" class="col-form-label col-md-3 text-right">start date</label>
                            <div class="col-md-9">
                                <div class="input-group" style="width: 100%">
                                    <fmt:message var="tooltip" key="motd.startDate.tooltip" />
                                    <form:input path="startDate" cssClass="form-control datetimepicker${status.error ? ' is-invalid' : '' }" placeholder="dd/MMM/yyyy h:mm a" data-target="#startDate" data-toggle="datetimepicker" title="${tooltip}" />
                                    <form:errors path="startDate" cssClass="invalid-feedback" role="alert" element="div" />
                                </div>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="endDate">
                        <div class="form-group row">
                            <label for="endDate" class="col-form-label col-md-3 text-right">end date</label>
                            <div class="col-md-9">
                                <div class="input-group" style="width: 100%">
                                    <fmt:message var="tooltip" key="motd.endDate.tooltip" />
                                    <form:input path="endDate" cssClass="form-control datetimepicker${status.error ? ' is-invalid' : '' }" placeholder="dd/MMM/yyyy h:mm a" data-target="#endDate" data-toggle="datetimepicker" title="${tooltip}" />
                                    <form:errors path="endDate" cssClass="invalid-feedback" role="alert" element="div" />
                                </div>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="resetAllShowMotd">
                        <div class="form-group row">
                            <div class="offset-md-3 col-md-6">
                                <div class="custom-control custom-checkbox">
                                    <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="resetAllShowMotd" />
                                    <label for="resetAllShowMotd1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="motd.resetAllShowMotd.tooltip" />"> reset all show motd</label>
                                </div>
                                <form:errors path="resetAllShowMotd" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary float-right">save</button>
                            </div>
                        </div>
                    </spring:bind>
                </form:form>

            </div>
        </div>
    </div>
    <script type="text/javascript">
					$.fn.datetimepicker.Constructor.Default = $.extend(
							$.fn.datetimepicker.Constructor.Default, {
								icons : {
									time : 'fas fa-clock',
									date : 'fas fa-calendar-alt',
									up : 'fas fa-arrow-up',
									down : 'fas fa-arrow-down',
									previous : 'fas fa-chevron-left',
									next : 'fas fa-chevron-right',
									today : 'fas fa-calendar-check',
									clear : 'fas fa-trash-alt',
									close : 'fas fa-times'
								}
							});

					$(document)
							.ready(
									function() {
										<c:if test="${empty motd.id}">
										doAction(
												'<c:url value="/admin/api/motd/limit" />',
												{
													method : 'GET'
												}, actions.limit);
										</c:if>

										<fmt:parseDate value="${motd.startDate}" pattern="yyyy-MM-dd'T'HH:mm" var="startDate" type="both" />
										<fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${startDate}" var="startDate" timeZone="${config.subject.timeZone}" />

										$('#startDate')
												.on(
														'change.datetimepicker',
														function(event) {
															$('#endDate')
																	.datetimepicker(
																			'minDate',
																			event.date);
														})
												.on(
														'focusout',
														function(event) {
															$('#startDate')
																	.datetimepicker(
																			'hide');
														});

										<fmt:parseDate value="${motd.endDate}" pattern="yyyy-MM-dd'T'HH:mm" var="endDate" type="both" />
										<fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${endDate}" var="endDate" timeZone="${config.subject.timeZone}" />

										// https://github.com/tempusdominus/bootstrap-4/issues/39
										$('#endDate').on(
												'focusout',
												function(event) {
													$('#endDate')
															.datetimepicker(
																	'hide');
												});

										$('#startDate').datetimepicker({
											format : 'DD/MMM/YYYY h:mm a'
										});
										$('#startDate')
												.datetimepicker(
														{
															date : moment(
																	'${startDate}',
																	'DD/MMM/YYYY h:mm a'),
															showToday : true,
															showClose : true,
														});

										$('#endDate').datetimepicker({
											format : 'DD/MMM/YYYY h:mm a'
										});
										$('#endDate')
												.datetimepicker(
														{
															date : moment(
																	'${endDate}',
																	'DD/MMM/YYYY h:mm a'),
															showClose : true,
														});

										$('#startDate').val('${startDate}');
										$('#endDate').val('${endDate}');

									});
				</script>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
