<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">profile</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">config</c:param>
    </c:import>

    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">edit profile</h4>
            <div class="card-body">

                <c:url var="action" value="/profile/save" />
                <form:form action="${action}" cssClass="form-horizontal" modelAttribute="config" method="POST">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a href="#profile" aria-controls="profile" role="tab" class="nav-link active" data-toggle="tab">profile</a>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a href="#settings" aria-controls="settings" role="tab" class="nav-link" data-toggle="tab">settings</a>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a href="#deactivate" aria-controls="deactivate" role="tab" class="nav-link" data-toggle="tab">deactivate</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="profile">

                            <spring:bind path="subject.username">
                                <div class="form-group row">
                                    <label for="subject.username" class="col-form-label col-md-3 text-right">email</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width: 100%">
                                            <fmt:message var="tooltip" key="config.username.tooltip" />
                                            <form:input path="subject.username" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="username" data-toggle="tooltip" title="${tooltip}" disabled="true" />
                                            <c:if test="${config.subject.social}">
                                                <span class="input-group-append"> <a href="<c:url value='/subject/disconnect' />" class="btn btn-danger" data-toggle="tooltip"
                                                    title="<fmt:message key='config.google.tooltip' />"><span class="fas fa-times" aria-hidden="true"></span> disconnect</a>
                                                </span>
                                            </c:if>
                                            <form:errors path="subject.username" cssClass="invalid-feedback" role="alert" element="div" />
                                        </div>
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="subject.joined">
                                <div class="form-group row">
                                    <label for="subject.joined" class="col-form-label col-md-3 text-right">joined date</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width: 100%">
                                            <fmt:message var="tooltip" key="subject.joined.tooltip" />
                                            <form:input path="subject.joined" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="joined date" data-toggle="tooltip" title="${tooltip}" disabled="true" />
                                            <form:errors path="subject.joined" cssClass="invalid-feedback" role="alert" element="div" />
                                        </div>
                                    </div>
                                </div>
                            </spring:bind>

                            <c:if test="${not config.subject.social}">
                                <c:if test="${not empty config.subject.password}">

                                    <spring:bind path="changePassword.oldPassword">
                                        <div class="form-group row">
                                            <label for="changePassword.oldPassword" class="col-form-label col-md-3 text-right">current password</label>
                                            <div class="col-md-9">
                                                <fmt:message var="tooltip" key="changePassword.oldPassword.tooltip" />
                                                <form:password path="changePassword.oldPassword" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="current password" data-toggle="tooltip" title="${tooltip}" />
                                                <form:errors path="changePassword.oldPassword" cssClass="invalid-feedback" role="alert" element="div" />
                                            </div>
                                        </div>
                                    </spring:bind>

                                </c:if>

                                <spring:bind path="changePassword.password.password1">
                                    <div class="form-group row">
                                        <label for="changePassword.password.password1" class="col-form-label col-md-3 text-right">new password</label>
                                        <div class="col-md-9">
                                            <fmt:message var="tooltip" key="password.password1.tooltip" />
                                            <form:password path="changePassword.password.password1" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="new password" data-toggle="tooltip" title="${tooltip}" />
                                            <form:errors path="changePassword.password.password1" cssClass="invalid-feedback" role="alert" element="div" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="changePassword.password.password2">
                                    <div class="form-group row">
                                        <label for="changePassword.password.password2" class="col-form-label col-md-3 text-right">new password again</label>
                                        <div class="col-md-9">
                                            <fmt:message var="tooltip" key="password.password2.tooltip" />
                                            <form:password path="changePassword.password.password2" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="new password again" data-toggle="tooltip" title="${tooltip}" />
                                            <form:errors path="changePassword.password.password2" cssClass="invalid-feedback" role="alert" element="div" />
                                        </div>
                                    </div>
                                </spring:bind>

                            </c:if>

                            <spring:bind path="name.givenName">
                                <div class="form-group row">
                                    <label for="name.givenName" class="col-form-label col-md-3 text-right">given name</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="name.givenName.tooltip" />
                                        <form:input path="name.givenName" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="given name" data-toggle="tooltip" title="${tooltip}" readonly="${config.subject.social}" />
                                        <form:errors path="name.givenName" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="name.familyName">
                                <div class="form-group row">
                                    <label for="name.familyName" class="col-form-label col-md-3 text-right">family name</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="name.familyName.tooltip" />
                                        <form:input path="name.familyName" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="family name" data-toggle="tooltip" title="${tooltip}" readonly="${config.subject.social}" />
                                        <form:errors path="name.familyName" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="subject.timeZone">
                                <div class="form-group row">
                                    <label for=subject.timeZone class="col-form-label col-md-3 text-right">time zone</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="subject.timeZone.tooltip" />
                                        <form:select path="subject.timeZone" items="${timeZones}" cssClass="form-control${status.error ? ' is-invalid' : '' }" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="subject.timeZone" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="settings">

                            <spring:bind path="useToolTips">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="useToolTips" />
                                            <label for="useToolTips1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="config.useToolTips.tooltip" />"> use tool tips </label>
                                        </div>
                                        <form:errors path="useToolTips" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="receiveEmail">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="receiveEmail" />
                                            <label for="receiveEmail1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="config.receiveEmail.tooltip" />"> receive email newsletter </label>
                                        </div>
                                        <form:errors path="receiveEmail" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="showMotd">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="showMotd" />
                                            <label for="showMotd1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="config.motd.tooltip" />"> show message of the day </label>
                                        </div>
                                        <form:errors path="showMotd" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="deactivate">

                            <div class="alert alert-danger">
                                <p>
                                    Here you can deactivate your account. The checkbox below will only activate if you have deleted all your <a href="<c:url value="/notebook" />" class="alert-link">Notebooks</a>.
                                </p>
                                <p>
                                    When you check the box and click <strong>save</strong>, you will receive an email with a link to deactivate your account. Once clicked the process cannot be stopped and we cannot recover your data.
                                </p>

                                <p>By following the link, your account details will be anonymised. Please understand that we cannot anonymise our automated backups. So it may take some time before all the data is rotated out of use.
                            </div>

                            <spring:bind path="deactivateAccount">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="deactivateAccount" disabled="${not config.subject.deactivatable}" />
                                            <label for="deactivateAccount1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="config.deactivateAccount.tooltip" />">
                                                <c:if test="${config.subject.deactivatable}">
                                                    <input type="hidden" name="_deactivateAccount" value="on">
                                                </c:if>
                                                deactivate account
                                            </label>
                                        </div>
                                        <form:errors path="deactivateAccount" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary float-right">save</button>
                        </div>
                    </div>

                </form:form>

            </div>
        </div>
    </div>

    <script type="text/javascript">
					$(document).ready(function() {
						<c:if test="${config.setProfile}">
						$('#subject\\.timeZone').val(moment.tz.guess());
						</c:if>

						<fmt:parseDate value="${config.subject.joined}" pattern="yyyy-MM-dd'T'HH:mm" var="joined" type="both" />
						<fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${joined}" var="joined" timeZone="${config.subject.timeZone}" />
						$('#subject\\.joined').val('${joined}');
					});
				</script>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
