package com.tetsuwantech.example.web.config;

import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.atom.web.authentication.PasswordModel;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ChangePasswordModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	private String oldPassword = new String();

	// don't set as @Valid
	private PasswordModel password = new PasswordModel();

	public ChangePasswordModel() {
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword
	 *            the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the passwordModel
	 */
	public PasswordModel getPassword() {
		return password;
	}

	/**
	 * @param passwordModel
	 *            the passwordModel to set
	 */
	public void setPassword(PasswordModel password) {
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ChangePasswordModel)) {
			return false;
		}

		return true;
	}

}
