---
-- #%L
-- example
-- %%
-- Copyright (C) 2018 Tetsuwan Technology
-- %%
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--      http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- #L%
---
-- To use this file update persistence.xml so that: 
-- hibernate.hbm2ddl.auto = create-drop
--
-- To update tables use
-- hibernate.hbm2ddl.auto = update

INSERT INTO motd (description, start_date, title) VALUES ('<span style="font-size: 24px;">Example dev environment.</span><br/><strong>username:</strong> ${atom.bcc}, <strong>password:</strong> Example!', date_trunc('minute', current_timestamp at time zone 'UTC'), 'dev warning');

--
-- Password is Example!
--

INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('${atom.bcc}', '$2a$10$aCgAuLJDd870JC37bxA/xOHSpohvX3iQ/gLIwCg8yEwdwATrX5hta', '2018-10-17 09:57:00', date_trunc('minute', current_timestamp at time zone 'UTC') + interval '1 year', 'Example', 'Application', 'Australia/Sydney', null, null, 'ENABLED');
INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_ADMIN', currval('subject_id_seq'));
INSERT INTO config (subject_id, use_tool_tips, set_profile, show_motd, receive_email) VALUES (currval('subject_id_seq'), false, false, false, false);


