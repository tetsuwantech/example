<#ftl encoding="UTF-8">
<#--
 #%L
 example
 %%
 Copyright (C) 2018 Tetsuwan Technology
 %%
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 #L%
-->
<p><strong>Welcome to Example Application.</strong></p>

<p>Thanks for signing up, your account has been activated and is ready to use, so head over to Example Application now and get started.</p> 
