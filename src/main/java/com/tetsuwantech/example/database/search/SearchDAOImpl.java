package com.tetsuwantech.example.database.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.hibernate.jpa.QueryHints;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.database.notebook.note.Note;

@Named(value = "SearchDAO")
public class SearchDAOImpl implements SearchDAO {

	// private static Logger LOG = LoggerFactory.getLogger(SearchDAOImpl.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	public List<? extends GenericEntity> search(Config config, Search search) {

		String text = search.getText();

		// private static Logger LOG = LoggerFactory.getLogger(SearchDAOImpl.class);
		FullTextEntityManager fullTextEntityManager = org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
		BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
		QueryBuilder queryBuilder;

		// Notebook
		if (search.getClasses().contains(Notebook.class)) {
			queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Notebook.class).get();
			Query notebookTextQuery = (StringUtils.isEmpty(text) ? queryBuilder.all() : queryBuilder.keyword().onFields("title", "description").matching(text)).createQuery();
			Query notebookWhereQuery = queryBuilder.keyword().onFields("config.id").matching(config.getId()).createQuery();
			Query notebookQuery = queryBuilder.bool().must(notebookTextQuery).must(notebookWhereQuery).createQuery();
			booleanQuery.add(notebookQuery, BooleanClause.Occur.SHOULD);
		}

		// Note
		if (search.getClasses().contains(Note.class)) {
			queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Note.class).get();
			Query noteTextQuery = (StringUtils.isEmpty(text) ? queryBuilder.all() : queryBuilder.keyword().onFields("title", "text").matching(text)).createQuery();
			Query noteWhereQuery = queryBuilder.keyword().onFields("notebook.config.id").matching(config.getId()).createQuery();
			Query noteQuery = queryBuilder.bool().must(noteTextQuery).must(noteWhereQuery).createQuery();
			booleanQuery.add(noteQuery, BooleanClause.Occur.SHOULD);
		}

		// save for lucene highlighter
		Query query = booleanQuery.build();
		search.setQuery(query);

		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, search.getClasses().toArray(new Class<?>[search.getClasses().size()]));
		fullTextQuery.setHint(QueryHints.HINT_CACHEABLE, true);

		fullTextQuery.setFirstResult(search.getFirstResult());
		fullTextQuery.setMaxResults(search.getMaxResults());

		// return for display
		search.setResultSize(fullTextQuery.getResultSize());

		@SuppressWarnings("unchecked")
		List<? extends GenericEntity> results = fullTextQuery.getResultList();

		return results;
	}

	@Override
	public void rebuild(Search search) {
		FullTextEntityManager fullTextEntityManager = org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
		fullTextEntityManager.createIndexer().start();
	}

}
