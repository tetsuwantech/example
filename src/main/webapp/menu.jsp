<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<c:set var="config" value="${sessionScope.config}" />

<sec:authorize access="hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
    <div class="alert alert-danger" style="margin-bottom: 0px; padding: 5px;">
        <strong>You are logged in as</strong>
        <sec:authentication property="principal.username" />
    </div>
</sec:authorize>

<sec:authorize access="isAuthenticated()">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">

        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/" />"> <img alt="Example Application Home" style="max-height: 24px;" class="img-fluid rounded" src="<c:url value="/img/example_logo.png" />">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-colapse" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-colapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item${param.menu eq 'notebook' ? ' active' : ''}">
                    <a class="nav-link" href="<c:url value="/notebook" />">notebooks</a>
                    <c:if test="${not empty notebook.id}">
                        <li class="nav-item${param.menu eq 'note' ? ' active' : ''}">
                            <a class="nav-link" href="<c:url value="/note" />?notebook.id=${notebook.id}">notes</a>
                        </li>
                    </c:if>
                </li>
            </ul>

            <a href="https://tetsuwan-tech.com/support/" target="_blank" class="btn btn-dark mr-2"><span class="fas fa-question-circle"></span> support</a>

            <c:url var="action" value="/search/save" />
            <form action="${action}" class="form-inline navbar-form navbar-right" role="search" method="POST">
                <sec:csrfInput />
                <div class="input-group">
                    <fmt:message var="tooltip" key="tooltip.search" />
                    <input name="text" class="form-control${status.error ? ' is-invalid' : '' }" placeholder="search" data-toggle="tooltip" title="${tooltip}" /> <span class="input-group-append">
                        <button class="btn btn-dark" type="submit" aria-hidden="true">
                            <span class="fas fa-search"></span>
                        </button>
                    </span>
                </div>
            </form>

            <ul class="navbar-nav navbar-right">

                <li class="nav-item dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding-top: 5px; padding-bottom: 4px;"> <sec:authentication var="username"
                            property="principal.username" /> <img class="rounded-circle" src="${config.subject.avatarUrl}${config.subject.social ? '=s32-c' : '?s=32&d=mp'}" alt="${username}" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <a class="dropdown-item" href="<c:url value="/admin/subject" />">subjects</a>
                            <a class="dropdown-item" href="<c:url value="/admin/motd" />">motds</a>
                            <div class="dropdown-divider"></div>
                        </sec:authorize>

                        <a class="dropdown-item" href="<c:url value="/profile" />">profile</a> <a class="dropdown-item" href="javascript:void(0);" onclick="doLogout();">logout</a>
                        <c:url var="action" value="/subject/logout" />
                        <form:form action="${action}" method="POST" id="logout" />
                        <script type="text/javascript">
							function doLogout() {
								$('#logout').submit();
							}
							</script>
                    </div>
                </li>
            </ul>

        </div>

    </nav>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <ol class="breadcrumb">
        <c:choose>
            <c:when test="${param.menu eq 'admin'}">
                <li class="breadcrumb-item">admin</li>
                <c:choose>
                    <c:when test="${not empty subjects}">
                        <li class="breadcrumb-item active">subjects</li>
                    </c:when>
                    <c:when test="${empty motd.id}">
                        <li class="breadcrumb-item active">motds</li>
                    </c:when>
                    <c:when test="${not empty motd.id}">
                        <li class="breadcrumb-item">
                            <a href="<c:url value="/admin/motd" />">motds</a>
                        </li>
                        <li class="breadcrumb-item active">${motd.title}</li>
                    </c:when>
                </c:choose>
            </c:when>

            <c:when test="${param.menu eq 'notebook'}">
                <c:choose>
                    <c:when test="${empty notebook.id}">
                        <li class="breadcrumb-item active">notebooks</li>
                    </c:when>
                    <c:when test="${not empty notebook.id}">
                        <li class="breadcrumb-item">
                            <a href="<c:url value="/notebook" />">notebooks</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <c:out value="${notebook.title}" />
                        </li>
                    </c:when>
                </c:choose>
            </c:when>
            <c:when test="${param.menu eq 'note'}">
                <li class="breadcrumb-item">
                    <a href="<c:url value="/notebook" />">notebooks</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<c:url value="/notebook/edit" />?notebook.id=${notebook.id}"><c:out value="${notebook.title}" /></a>
                </li>
                <c:choose>
                    <c:when test="${empty note.id}">
                        <li class="breadcrumb-item">notes</li>
                    </c:when>
                    <c:otherwise>
                        <li class="breadcrumb-item">
                            <a href="<c:url value="/note" />?notebook.id=${notebook.id}">notes</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <c:out value="${note.title}" />
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${param.menu eq 'config'}">
                <li class="breadcrumb-item">profile</li>
            </c:when>
            <c:when test="${param.menu eq 'search'}">
                <li class="breadcrumb-item">search</li>
            </c:when>
            <c:otherwise>
                <li class="breadcrumb-item">error</li>
            </c:otherwise>
        </c:choose>
    </ol>
</sec:authorize>

<div class="container">
    <c:if test="${not empty messageOfTheDay}">
        <div class="alert alert-dismissable alert-info fade show text-center" data-type="messageOfTheDay" style="display: none;"
            onclose="doAction('<c:url value="/api/profile/motd" />', { throbber: false }, function() { $('[data-type=messageOfTheDay]').hide('fade'); })">
            <sec:authorize access="isAuthenticated()">
                <button type="button" class="close" data-dismiss="alert" aria-label="close"></button>
            </sec:authorize>
            ${messageOfTheDay}
        </div>
    </c:if>

    <div data-type="ajax-message"></div>

    <c:set var="alert" value="${not empty type or not empty param.type ? (not empty type ? type : param.type) : 'success'}" />
    <div data-type="message" class="alert alert-${alert} alert-dismissible fade show" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="close"></button>
        <c:choose>
            <c:when test="${alert eq 'danger'}">
                <span class="fas fa-exclamation-triangle" aria-hidden="true"></span>
            </c:when>
            <c:when test="${alert eq 'warning'}">
                <span class="fas fa-exclamation-circle" aria-hidden="true"></span>
            </c:when>
            <c:when test="${alert eq 'info'}">
                <span class="fas fa-info-circle" aria-hidden="true"></span>
            </c:when>
            <c:otherwise>
                <span class="fas fa-check-circle" aria-hidden="true"></span>
            </c:otherwise>
        </c:choose>
        <span data-type="alert-text"></span>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	<c:if test="${not empty message}">
		$('[data-type=alert-text]').html('<fmt:message key="${message}"><c:forEach var="arg" items="${args}"><fmt:param value="${fn:replace(arg, '\\\'', '\\\\\\'')}" /></c:forEach></fmt:message>');
		$('[data-type=message]').show();
	</c:if>
	
	<c:if test="${empty config.showMotd or config.showMotd}">
		$('[data-type=messageOfTheDay]').show();
	</c:if>

	<c:choose>
		<c:when test="${config.setProfile and param.menu ne 'config'}">
			$('#modal-title').html( 'Welcome <sec:authentication property="principal.givenName" />!' );
			$('#modal-body').html( '<p>As this is your first visit, we\'d like to make sure your profile is up to date.</p><p>Let\'s go to the profile page and set you up.</p>' );
			$('#modal-footer').html( '<a href="<c:url value="/profile" />" class="btn btn-primary" role="button" data-toggle="tooltip" title="<fmt:message key="tooltip.takeMeThere" />"><span class="fas fa-arrow-right" aria-hidden="true"></span> <fmt:message key="tooltip.takeMeThere" /></a>' );
			$('#modal').modal('show');
		</c:when>
		<c:when test="${not empty param.modalBody}">
			doModal('Hey there, <sec:authentication property="principal.givenName" />', '${fn:replace(param.modalBody, '\'', '\\\'')}', '<c:url value="${param.modalUrl}" />', ${param.modalRequired});
		</c:when>
		<c:otherwise>
			$('#modal').modal('hide');
		</c:otherwise>
	</c:choose>
});

function doModal(title, body, url, required) {
	
	var buttons = required ? '' : '<button data-dismiss="modal" class="btn btn-primary" type="button" data-toggle="tooltip" title="<fmt:message key="tooltip.noThanks" />"><span class="fas fa-times" aria-hidden="true"></span> <fmt:message key="tooltip.noThanks" /></button>';
	buttons += '<a href="'+url+'"';
	buttons += required ? '' : ' onclick="$(\'#modal\').modal(\'hide\');"';
	buttons += ' class="btn btn-primary" role="button" data-toggle="tooltip" title="';
	buttons += required ? '<fmt:message key="tooltip.takeMeThere" />"' : '<fmt:message key="tooltip.showMe" />" target="_blank"';
	buttons += '><span class="fas fa-arrow-right" aria-hidden="true"></span> ';
	buttons += required ? '<fmt:message key="tooltip.takeMeThere" />' : '<fmt:message key="tooltip.showMe" />';
	buttons += '</a>';

	$('#modal-title').html(title);
	$('#modal-body').html(body);
	$('#modal-footer').html(buttons);
	$('#modal').modal('show');
}

</script>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal-title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="close"></button>
            </div>
            <div id="modal-body" class="modal-body"></div>
            <div id="modal-footer" class="modal-footer"></div>
        </div>
    </div>
</div>
