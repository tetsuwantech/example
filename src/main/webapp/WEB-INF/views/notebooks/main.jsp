<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">notebook</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">notebook</c:param>
        <c:param name="modalBody">
            <c:if test="${empty notebooks}">
                <fmt:message key="notebooks.doc.empty" />
            </c:if>
        </c:param>
        <c:param name="modalUrl">
            <c:if test="${empty notebooks}">
                <fmt:message key="notebooks.doc.url" />
            </c:if>
        </c:param>
    </c:import>

    <div id="main" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">
                notebooks
                <a href="<c:url value="/notebook/create" />" class="btn btn-primary btn-sm float-right" type="button" data-toggle="tooltip" title="<fmt:message key="notebook.create.tooltip" />"> <span class="fas fa-plus" aria-hidden="true"></span>
                    create notebook
                </a>
            </h4>
            <div class="card-body">

                <form id="reorder" action="<c:url value='/api/notebook/reorder' />" method="POST">
                    <div id="sortable" class="row">
                        <c:forEach var="notebook" items="${notebooks}" varStatus="forEach">

                            <div data-type="notebook" data-id="${notebook.id}" class="col-md-3 mb-4">
                                <div class="card">
                                    <input type="hidden" name="id" value="${notebook.id}" /> <input type="hidden" name="sequence" value="${notebook.sequence}" />

                                    <div class="card-header">
                                        <a href="<c:url value="/notebook/edit" />?id=${notebook.id}" data-toggle="tooltip" title="<fmt:message key="tooltip.edit" />"><c:out value="${notebook.title}" /></a>
                                    </div>
                                    <div class="card-body">
                                        <c:if test="${notebook.deletable}">
                                            <div class="float-right">
                                                <button onclick="doAction('<c:url value="/api/notebook/remove" />', { method: 'DELETE', type: 'notebook', id: ${notebook.id}}, actions.remove);" class="btn btn-danger btn-sm" type="button"
                                                    data-toggle="tooltip" title="<fmt:message key="tooltip.delete" />">
                                                    <span class="fas fa-trash" aria-hidden="true"></span>
                                                    <fmt:message key="tooltip.delete" />
                                                </button>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>

                        </c:forEach>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
