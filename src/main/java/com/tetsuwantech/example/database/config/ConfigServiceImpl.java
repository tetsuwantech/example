package com.tetsuwantech.example.database.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import com.tetsuwantech.atom.database.GenericServiceImpl;
import com.tetsuwantech.atom.database.authentication.Subject;

@Transactional
@Named(value = "ConfigService")
public class ConfigServiceImpl extends GenericServiceImpl<Config, Subject> implements ConfigService {

	@Override
	public Config get(Long id) {

		Config config = super.get(id);

		config.getSubject().getAuthorities().size();

		return config;
	}

	@Override
	public Config findFirst(Subject subject) {
		Config config = super.findFirst(subject);

		if (config != null) {
			config.getSubject().getAuthorities().size();
		}

		return config;

	}

	@Override
	public List<Config> findAll(Subject where, String column, String orderBy) {

		List<Config> configEntities = super.findAll(where, column, orderBy);
		configEntities.forEach(config -> {
			config.getSubject().getAuthorities().size();
		});

		return configEntities;
	}

	@Override
	public void resetAllShowMotd() {
		List<Config> configEntities = super.findAll(null, null, null);
		configEntities.forEach(config -> {
			config.setShowMotd(true);
			super.save(config);
		});
	}
}
