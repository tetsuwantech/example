<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">subject</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">admin</c:param>
    </c:import>

    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">subjects</h4>
            <div class="card-body">

                <table class="table table-hover table-striped table-sm" data-toggle="table" data-id-field="id" data-search="true" data-show-footer="true">
                    <thead class="thead-dark">
                        <tr>
                            <th data-field="id" data-sortable="true" class="text-center">#</th>
                            <th data-field="username" data-sortable="true" class="text-center">username</th>
                            <th data-field="displayName" data-sortable="true" class="text-center">display name</th>
                            <th data-field="joined" data-sortable="true" data-sorter="dateSorter" class="text-center">date joined</th>
                            <th data-field="lastLogin" data-sortable="true" data-sorter="dateSorter" class="text-center">last login</th>
                            <th data-field="credentialsNonExpired" class="text-center">password<br />not expired
                            </th>
                            <th data-field="accountNonExpired" class="text-center">account<br />not expired
                            </th>
                            <th data-field="status" data-footer-formatter="totalCount" class="text-center">status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="subject" items="${subjects}">
                            <tr>
                                <td>${subject.id}</td>
                                <td class="text-left"><a href="#" onclick="doAction('<c:url value="/admin/login" />?username=${subject.username}', { method: 'POST' });"> <c:out value="${subject.username}" /></a></td>
                                <td class="text-left"><c:out value="${subject.givenName}" /> <c:out value="${subject.familyName}" /></td>
                                <td><fmt:parseDate value="${subject.joined}" pattern="yyyy-MM-dd'T'HH:mm" var="joined" type="both" /> <fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${joined}" timeZone="${config.subject.timeZone}" /></td>
                                <td><fmt:parseDate value="${subject.lastLogin}" pattern="yyyy-MM-dd'T'HH:mm" var="lastLogin" type="both" /> <fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${lastLogin}" timeZone="${config.subject.timeZone}" /></td>
                                <td class="text-center"><span class="badge badge-${subject.credentialsNonExpired ? 'success' : 'danger'}"> <c:out value="${subject.credentialsNonExpired}" />
                                </span></td>
                                <td class="text-center"><span class="badge badge-${subject.accountNonExpired ? 'success' : 'danger'}"> <c:out value="${subject.accountNonExpired}" />
                                </span></td>
                                <td class="text-center"><c:choose>
                                        <c:when test="${subject.status eq 'EXPIRED'}">
                                            <c:set var="context" value="danger" />
                                        </c:when>
                                        <c:when test="${subject.status eq 'PENDING'}">
                                            <c:set var="context" value="info" />
                                        </c:when>
                                        <c:when test="${subject.status eq 'LOCKED'}">
                                            <c:set var="context" value="warning" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="context" value="success" />
                                        </c:otherwise>
                                    </c:choose> <span class="badge badge-${context}"> <c:out value="${subject.status}" />
                                </span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script type="text/javascript">
					function dateSorter(a, b) {
						var a = a.trim(), b = b.trim();
						return a.length && b.length ? moment(a,
								'DD/MMM/YYYY h:mm a').diff(
								moment(b, 'DD/MMM/YYYY h:mm a')) : a.length
								- b.length;
					}

					function totalCount(data) {
						return '<strong>total ' + data.length + '</strong>'
					}
				</script>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
