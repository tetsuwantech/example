package com.tetsuwantech.example.web.notebook;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tetsuwantech.example.manager.notebook.NotebookManager;
import com.tetsuwantech.example.web.ControllerHelper;
import com.tetsuwantech.example.web.ControllerHelper.Mapping;
import com.tetsuwantech.example.web.config.ConfigModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes(value = "config")
public class NotebookController {

	// private static Logger LOG =
	// LoggerFactory.getLogger(NotebookController.class);

	@Inject
	private NotebookManager notebookManager;

	@Inject
	private MessageSource messageSource;

	private ControllerHelper<NotebookManager, NotebookModel, ?> controllerHelper;

	@PostConstruct
	private void init() {
		controllerHelper = new ControllerHelper<>(notebookManager, Mapping.NOTEBOOK, messageSource);
	}

	@ModelAttribute
	public void populateModel(@SessionAttribute(name = "config") ConfigModel config, @ModelAttribute(name = "notebook") NotebookModel notebook, Model model) {

		if (notebook.getId() != null) {
			// grab the data
			notebook = notebookManager.get(notebook.getId());
			model.addAttribute("notebook", notebook);
			model.addAttribute("args", new String[] { notebook.getTitle() });
		}

		if (notebook.getId() == null) {
			model.addAttribute("notebooks", notebookManager.findAll(config));
		}
	}

	// we don't force method = RequestMethod.GET here because it might be getting a
	// 403 redirect with status
	@GetMapping(value = "/notebook")
	public String main(@RequestParam(required = false) String status, Model model) {

		// need to add new notebookModel as it will be still stuck from session
		model.addAttribute("notebook", new NotebookModel());

		if (status != null) {
			model.addAttribute("message", "subjects." + status);
		}

		return "/notebooks/main";
	}

	@GetMapping(value = "/notebook/create")
	public String create(Model model) {
		model.addAttribute("message", "notebooks.edit.create");
		model.addAttribute("type", "info");
		return "/notebooks/edit";
	}

	@GetMapping(value = "/notebook/edit")
	public String edit(Model model) {

		// a Notebook will always be defined - they cannot be created here
		model.addAttribute("message", "notebooks.edit.edit");
		model.addAttribute("type", "info");
		return "/notebooks/edit";
	}

	@DeleteMapping(value = "/api/notebook/remove")
	public @ResponseBody String remove(@SessionAttribute(name = "config") ConfigModel config, @RequestBody NotebookModel notebook, Model model) {
		return controllerHelper.remove(config, notebook);
	}

	@PostMapping(value = "/api/notebook/reorder")
	public @ResponseBody String reorder(@RequestBody List<NotebookModel> notebooks, Model model) {
		return controllerHelper.reorder(notebooks);
	}

	@PostMapping(value = "/notebook/save")
	public String save(@SessionAttribute(name = "config") ConfigModel config, @Valid @ModelAttribute(name = "notebook") NotebookModel notebook, BindingResult result, Model model) {

		notebook.setConfig(config);

		if (result.hasErrors()) {
			model.addAttribute("message", "errors");
			model.addAttribute("type", "danger");
		} else {

			// save away - magically
			notebookManager.save(notebook);
			populateModel(config, notebook, model);

			model.addAttribute("message", "notebooks.edit.save");
		}

		return "/notebooks/edit";
	}
}
