package com.tetsuwantech.example.manager.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.scheduling.annotation.Async;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.config.ConfigService;
import com.tetsuwantech.example.web.config.ConfigModel;

@Named(value = "ConfigManager")
public class ConfigManager extends GenericManager<Config, Subject, ConfigModel, SubjectModel> {

	@Inject
	private ConfigService configService;

	@Inject
	private SubjectService subjectService;

	@Override
	public ConfigModel createModel(Config config) {
		return new ConfigModel(config);
	}

	@Override
	public void save(ConfigModel configModel) {

		// if we're creating a new User we need to know
		Config config = null;
		boolean isNew = false;
		if (configModel.getId() == null) {
			config = new Config();
			Subject subject = subjectService.get(configModel.getSubject().getId());
			config.setSubject(subject);

			isNew = true;
		} else {
			config = configService.get(configModel.getId());
		}

		config.setSetProfile(configModel.getSetProfile());
		config.setUseToolTips(configModel.getUseToolTips());
		config.setShowMotd(configModel.getShowMotd());
		config.setReceiveEmail(configModel.getReceiveEmail());

		if (isNew) {
			configService.create(config);
			configModel.setId(config.getId());
		} else {
			configService.save(config);
		}
	}

	@Override
	public List<ConfigModel> findAll(SubjectModel subjectModel) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public Long countAll(SubjectModel subjectModel) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Not supported.");
	}

	public ConfigModel findFirst(SubjectModel subjectModel) {
		Subject subject = subjectService.get(subjectModel.getId());
		Config config = null;
		if (subject != null) {
			config = configService.findFirst(subject);
		}

		return config == null ? null : new ConfigModel(config);
	}

	@Override
	public List<ConfigModel> search(SubjectModel subjectModel, String text) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Async
	public void resetAllShowMotd() {
		configService.resetAllShowMotd();
	}
}
