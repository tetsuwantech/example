<#ftl encoding="UTF-8">
<#--
 #%L
 example
 %%
 Copyright (C) 2018 Tetsuwan Technology
 %%
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 #L%
-->
<p>You (or someone) used the Forgot Password form on Example Application.

You can use this link to change your password:</p>

<p><a href="${url}">${url}</a></p>

<p>The link will expire in 24 hours.</p>

<p>If you did not request for your password to be changed, you can safely ignore this message.</p>
