package com.tetsuwantech.example.manager;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.manager.config.ConfigManager;
import com.tetsuwantech.example.manager.notebook.NotebookManager;
import com.tetsuwantech.example.web.config.ConfigModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

public class SubjectHandlerImpl implements SubjectHandler {

	// private static Logger LOG =
	// LoggerFactory.getLogger(SubjectHandlerImpl.class);

	@Inject
	private SubjectManager subjectManager;

	@Inject
	private ConfigManager configManager;

	@Inject
	private NotebookManager notebookManager;

	@Inject
	private DateTimePeriodUtils dateTimePeriodUtils;

	@Override
	public void build(SubjectModel subject, Map<String, String> map) {

		// stick with defaults for now
		ConfigModel config = new ConfigModel();
		config.setSubject(subject);

		configManager.save(config);

	}

	@Override
	public void login(SubjectModel subject, HttpServletRequest request, HttpServletResponse response) {

		ConfigModel config = configManager.findFirst(subject);

		// update last login time
		subject.setLastLogin(dateTimePeriodUtils.now());
		subjectManager.save(subject);

		// pull config again (has all the new data) and set up in session
		config = configManager.get(config.getId());
		config.setSubject(subject);
		request.getSession().setAttribute("config", config);
	}

	@Override
	public boolean isDeactivatable(SubjectModel subject) {

		// check there no content
		ConfigModel config = configManager.findFirst(subject);
		boolean noNotebooks = notebookManager.count(config) == 0;

		return noNotebooks;
	}

	@Override
	public void deactivate(SubjectModel subject) {
		ConfigModel config = configManager.findFirst(subject);
		configManager.remove(config.getId());
	}
}
