package com.tetsuwantech.example.database.notebook.note;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import com.tetsuwantech.atom.database.Deletable;
import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;
import com.tetsuwantech.atom.database.authentication.Securable;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.example.database.Sequenceable;
import com.tetsuwantech.example.database.notebook.Notebook;

/**
 * Entity implementation class for Entity: Notebook
 *
 */
@Entity
@Cacheable
@Indexed(index = "note")
@Table(name = "note")
public class Note extends GenericEntity implements Securable, Sequenceable, Deletable {

	private static final long serialVersionUID = 1L;

	@IndexedEmbedded(includePaths = { "config.id" })
	@Auditable
	@ManyToOne(targetEntity = Notebook.class)
	@JoinColumn(name = "notebook_id", updatable = false)
	private Notebook notebook;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Auditable
	@Column(length = LONG_LENGTH)
	@NotNull
	@Size(min = 1, max = LONG_LENGTH)
	private String title;

	@Auditable
	@NotNull
	@Min(value = 0)
	private Long sequence;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Analyzer(definition = "html_analyzer")
	@Auditable
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String text;

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public Long getSequence() {
		return sequence;
	}

	@Override
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public Subject getSubject() {
		return getNotebook().getConfig().getSubject();
	}

	@Override
	public boolean isDeletable() {
		return true;
	}

}
