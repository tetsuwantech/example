package com.tetsuwantech.example.web.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class SearchModelValidator implements Validator {

	@Override
	public boolean supports(Class<?> target) {
		return SearchModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {
		@SuppressWarnings("unused")
		SearchModel search = (SearchModel) object;
	}
}
