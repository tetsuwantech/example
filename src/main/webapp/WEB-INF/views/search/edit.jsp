<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">search</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">search</c:param>
    </c:import>

    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">
                search
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <a href="<c:url value='/admin/search/rebuild' />" class="btn btn-info btn-sm float-right" role="button" data-toggle="tooltip" title="<fmt:message key="tooltip.admin.rebuild" />"> <span class="fas fa-sync" aria-hidden="true"></span>
                        <fmt:message key="tooltip.admin.rebuild" />
                    </a>
                </sec:authorize>
            </h4>
            <div class="card-body">

                <c:url var="action" value="/search/save" />
                <form:form action="${action}" cssClass="form-horizontal" modelAttribute="search" method="POST">
                    <form:hidden path="pageNumber" />

                    <spring:bind path="text">
                        <div class="form-group row">
                            <label for="login" class="col-form-label col-md-3 text-right">text</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <fmt:message var="tooltip" key="search.text.tooltip" />
                                    <form:input path="text" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="search" data-toggle="tooltip" title="${tooltip}" />
                                    <span class="input-group-append">
                                        <button class="btn bg-dark text-white" onclick="$('#pageNumber').val(1);" type="submit" aria-hidden="true">
                                            <span class="fas fa-search"></span>
                                        </button>
                                    </span>
                                </div>
                                <form:errors path="text" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="filters">
                        <div class="form-group row">
                            <label for="filters" class="col-form-label col-md-3 text-right">search filter</label>
                            <div class="col-md-9">
                                <fmt:message var="tooltip" key="search.filters.tooltip" />
                                <form:select path="filters" id="filters" items="${filters}" itemLabel="name" cssClass="form-control${status.error ? ' is-invalid' : '' }" data-toggle="tooltip" title="${tooltip}" />
                                <form:errors path="filters" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                </form:form>

                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty results}">
                            <div class="row" style="border-bottom: 1px solid #ccc;">
                                <div class="offset-md-3 col-md-9">
                                    <p>
                                        Page <strong class="text-success">${search.pageNumber}</strong> of <strong class="text-success">${search.totalPages}</strong>. Showing <strong class="text-success">${search.resultSize}</strong> result
                                        <c:if test="${search.resultSize gt 1}">s</c:if>
                                        .
                                    </p>
                                </div>
                            </div>
                            <c:forEach var="result" items="${results}">

                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            <span class="badge badge-primary" style="margin-right: 5px;"><c:out value="${fn:substringBefore(result.getClass().getSimpleName(), 'Model')}" /></span>
                                            <a href="<c:url value="${result.searchURL}" />"><c:out value="${result.searchPath}" /></a>
                                        </h5>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p>${result.searchDescription}</p>
                                    </div>
                                </div>

                            </c:forEach>
                            <c:if test="${search.totalPages gt 1}">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item${search.pageNumber eq 1 ? ' disabled' : ''}">
                                        <a href="javascript:void(0);" onclick="doSubmit(${search.pageNumber - 1});" class="page-link"><span class="fas fa-arrow-left"></span> previous</a>
                                    </li>
                                    <li class="page-item${search.pageNumber eq search.totalPages ? ' disabled' : ''}">
                                        <a href="javascript:void(0);" onclick="doSubmit(${search.pageNumber + 1});" class="page-link">next <span class="fas fa-arrow-right"></span></a>
                                    </li>
                                </ul>
                            </c:if>
                        </c:if>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
$( "#filters" ).select2({
    theme: "bootstrap"
});

function doSubmit(page) {
	$('#pageNumber').val(page);
	$('#search').submit();
}
</script>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
