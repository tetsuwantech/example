<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">motd</c:param>
</c:import>
<c:set var="config" value="${sessionScope.config}" />

<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">admin</c:param>
    </c:import>

    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">
                motds <a href="<c:url value="/admin/motd/create" />" class="btn btn-primary btn-sm float-right" type="button" data-toggle="tooltip" title="<fmt:message key="motd.create.tooltip" />"><span class="fas fa-plus" aria-hidden="true"></span>
                    create motd </a>
            </h4>
            <div class="card-body">

                <c:forEach var="motd" items="${motds}">

                    <div data-type="motd" data-id="${motd.id}" class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="float-right">
                                    <%-- http://stackoverflow.com/questions/35606551/jstl-localdatetime-format --%>
                                    <fmt:parseDate value="${motd.startDate}" pattern="yyyy-MM-dd'T'HH:mm" var="startDate" type="both" />
                                    <fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${startDate}" timeZone="${config.subject.timeZone}" />
                                    <c:if test="${not empty motd.endDate}"> ~ 
                    		       		<fmt:parseDate value="${motd.endDate}" pattern="yyyy-MM-dd'T'HH:mm" var="endDate" type="both" />
                                        <fmt:formatDate pattern="dd/MMM/yyyy h:mm a" value="${endDate}" timeZone="${config.subject.timeZone}" />
                                    </c:if>
                                </div>
                                <a href="<c:url value="/admin/motd/edit" />?id=${motd.id}" class="btn-block" data-toggle="tooltip" title="${motd.title}"><c:out value="${motd.title}" /></a>
                            </div>
                            <div class="card-body">
                                <div class="alert alert-info fade show text-center">${motd.description}</div>
                                <div class="float-right">
                                    <button onclick="doAction('<c:url value="/admin/api/motd/remove" />', { method: 'DELETE', type: 'motd', id: ${motd.id}}, actions.remove);" class="btn btn-danger btn-sm" type="button" data-toggle="tooltip"
                                        title="<fmt:message key="tooltip.delete" />">
                                        <span class="fas fa-trash" aria-hidden="true"></span>
                                        <fmt:message key="tooltip.delete" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </c:forEach>

            </div>
        </div>
    </div>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
