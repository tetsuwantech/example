package com.tetsuwantech.example.database.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.Query;

public class Search {

	private String text;
	private List<Class<?>> classes = new ArrayList<>();

	private Query query;

	private int firstResult;
	private int maxResults;
	private int resultSize;

	public static final String ELLIPSIS = " ... ";
	public static final int MAX_NUMBER_OF_FRAGMENTS = 255;
	public static final int FRAGMENT_SIZE = 80;
	public static final int LONG_FRAGMENT_SIZE = 512;
	public static final String HIGHLIGHT_OPEN = "<mark>";
	public static final String HIGHLIGHT_CLOSE = "</mark>";

	/**
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the fields
	 */
	public List<Class<?>> getClasses() {
		return this.classes;
	}

	/**
	 * @param classes the fields to set
	 */
	public void setClasses(List<Class<?>> classes) {
		this.classes.clear();
		this.classes.addAll(classes);
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

}
