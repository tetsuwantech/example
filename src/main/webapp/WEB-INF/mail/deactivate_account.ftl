<#ftl encoding="UTF-8">
<#--
 #%L
 example
 %%
 Copyright (C) 2018 Tetsuwan Technology
 %%
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 #L%
-->
<p>You (or someone) started the deactivate account process on Example Application.</p>

<p>You can use this link to deactivate your account:</p>

<p><a href="${url}">${url}</a></p>

<p>The link will expire in 24 hours.</p>

<p>We'll be sorry to see you go.</p>

<p>Once you have clicked the link the process cannot be stopped. Your username/password will no longer work and you will need to re-register.</p>

<p>By following the link, your account details will be anonymised. Please understand that we cannot anonymise our automated backups. So it may take some time before all the data is rotated out of use.</p> 

<p>If you did not request for your account to be deactivated, you can safely ignore this message.</p>
