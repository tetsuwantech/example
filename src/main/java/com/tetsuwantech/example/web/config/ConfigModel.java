package com.tetsuwantech.example.web.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.validation.Valid;

import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.atom.web.authentication.NameModel;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.database.config.Config;

public class ConfigModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@Valid
	private NameModel name = new NameModel();

	private Boolean useToolTips = true;
	private Boolean setProfile = true;
	private Boolean showMotd = true;
	private Boolean receiveEmail = true;
	private Boolean deactivate = false;

	private SubjectModel subject;

	// don't set as @Valid
	private ChangePasswordModel changePassword = new ChangePasswordModel();

	public ConfigModel() {
	}

	public ConfigModel(Long id) {
		super(id);
	}

	public ConfigModel(Config config) {
		super(config);

		getName().setGivenName(config.getSubject().getGivenName());
		getName().setFamilyName(config.getSubject().getFamilyName());

		setUseToolTips(config.getUseToolTips());
		setShowMotd(config.getShowMotd());
		setReceiveEmail(config.getReceiveEmail());
		setSetProfile(config.getSetProfile());

	}

	/**
	 * @return the nameModel
	 */
	public NameModel getName() {
		return name;
	}

	/**
	 * @param name
	 *            the nameModel to set
	 */
	public void setName(NameModel name) {
		this.name = name;
	}

	/**
	 * @return the useToolTips
	 */
	public Boolean getUseToolTips() {
		return useToolTips;
	}

	/**
	 * @param useToolTips
	 *            the useToolTips to set
	 */
	public void setUseToolTips(Boolean useToolTips) {
		this.useToolTips = useToolTips;
	}

	public void setSetProfile(Boolean setProfile) {
		this.setProfile = setProfile;
	}

	public Boolean getSetProfile() {
		return this.setProfile;
	}

	/**
	 * @return the motd
	 */
	public Boolean getShowMotd() {
		return showMotd;
	}

	/**
	 * @param showMotd
	 *            the motd to set
	 */
	public void setShowMotd(Boolean showMotd) {
		this.showMotd = showMotd;
	}

	/**
	 * @return the receiveEmail
	 */
	public Boolean getReceiveEmail() {
		return receiveEmail;
	}

	/**
	 * @param receiveEmail
	 *            the receiveEmail to set
	 */
	public void setReceiveEmail(Boolean receiveEmail) {
		this.receiveEmail = receiveEmail;
	}

	/**
	 * @return the deactivate
	 */
	public Boolean getDeactivateAccount() {
		return false;
	}

	public void setDeactivateAccount(Boolean deactivateAccount) {
		this.deactivate = deactivateAccount;
	}

	public Boolean getDeactivate() {
		return this.deactivate;
	}

	/**
	 * @return the subject
	 */
	public SubjectModel getSubject() {
		return this.subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(SubjectModel subject) {
		this.subject = subject;
	}

	/**
	 * @return the changePasswordModel
	 */
	public ChangePasswordModel getChangePassword() {
		return changePassword;
	}

	/**
	 * @param changePassword
	 *            the changePasswordModel to set
	 */
	public void setChangePassword(ChangePasswordModel changePassword) {
		this.changePassword = changePassword;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ConfigModel)) {
			return false;
		}

		return true;
	}

}
