package com.tetsuwantech.example.web.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

public class ConfigModelValidator implements Validator {

	// private static Logger LOG =
	// LoggerFactory.getLogger(ConfigModelValidator.class);

	private final Validator changePasswordModelValidator;
	private final SubjectHandler subjectHandler;

	public ConfigModelValidator(BCryptPasswordEncoder bCryptPasswordEncoder, SubjectHandler subjectHandler) {
		this.changePasswordModelValidator = new ChangePasswordModelValidator(bCryptPasswordEncoder);
		this.subjectHandler = subjectHandler;
	}

	@Override
	public boolean supports(Class<?> target) {
		return ConfigModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ConfigModel config = (ConfigModel) object;

		// check if account can be deactivated
		SubjectModel subjectModel = SubjectUtils.getSubject();
		if (config.getDeactivate() && subjectHandler != null && !subjectHandler.isDeactivatable(subjectModel)) {
			errors.rejectValue("deactivateAccount", "Config.deactivateAccount");
		}

		errors.pushNestedPath("changePassword");
		ValidationUtils.invokeValidator(this.changePasswordModelValidator, config.getChangePassword(), errors);
		errors.popNestedPath();

	}
}
