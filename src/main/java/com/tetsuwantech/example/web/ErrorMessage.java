package com.tetsuwantech.example.web;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.context.MessageSource;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

public class ErrorMessage {

	// private static Logger LOG = LoggerFactory.getLogger(ErrorMessage.class);

	private String message;
	private String key;
	private MessageSource messageSource;

	public ErrorMessage() {
	}

	public ErrorMessage(String message, String key, MessageSource messageSource) {
		setMessage(message);
		setKey(key);
		setMessageSource(messageSource);
	}

	/**
	 * @return the key
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setMessage(String key) {
		this.message = key;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return messageSource.getMessage(key, new String[] { message }, null);
	}

}
