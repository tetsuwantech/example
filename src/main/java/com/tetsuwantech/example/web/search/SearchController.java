package com.tetsuwantech.example.web.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.example.manager.config.ConfigManager;
import com.tetsuwantech.example.manager.search.SearchManager;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.search.SearchModel.Filters;

@Controller
public class SearchController {

	// private static Logger LOG = LoggerFactory.getLogger(SearchController.class);

	@Inject
	ConfigManager configManager;

	@Inject
	SearchManager searchManager;

	@InitBinder(value = "search")
	private void initBinder(WebDataBinder binder) {
		binder.addValidators(new SearchModelValidator());
	}

	@ModelAttribute
	public void populateModel(@ModelAttribute(name = "search") SearchModel search, Model model) {

		// add the values for the filters
		model.addAttribute("filters", Filters.values());
	}

	@GetMapping(value = { "/search", "/search/edit" })
	public String edit(Model model) {
		model.addAttribute("message", "search.edit.edit");
		model.addAttribute("type", "info");
		return "/search/edit";
	}

	@Transactional
	@PostMapping(value = "/search/save")
	public String save(@Valid @ModelAttribute(name = "search") SearchModel search, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("message", "errors");
			model.addAttribute("type", "danger");
		} else {

			ConfigModel config = configManager.findFirst(SubjectUtils.getSubject());
			List<?> results = searchManager.search(config, search);

			model.addAttribute("results", results);

			if (results.isEmpty()) {
				model.addAttribute("message", "search.edit.no_results");
				model.addAttribute("type", "warning");
			} else {
				model.addAttribute("message", "search.edit.results");
				model.addAttribute("type", "info");
			}
		}

		return "/search/edit";
	}

	@GetMapping(value = "/admin/search/rebuild")
	public String rebuild(Model model) {

		searchManager.rebuild();
		model.addAttribute("message", "search.edit.rebuild");
		model.addAttribute("type", "info");

		return "/search/edit";
	}
}
