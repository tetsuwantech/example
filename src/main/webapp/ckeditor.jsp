<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<fmt:setLocale value="en_AU"/>
<fmt:setBundle basename="example-messages" />

<script type="text/javascript">
// Replace the <textarea id="editor"> with a CKEditor
CKEDITOR.replace( 'editor',
{
    customConfig: '',
    height: '350px',
    extraPlugins: 'image2,justify,lineutils,widget,colorbutton',
    removePlugins: 'image,scayt',
    contentsCss: '<c:url value="/css/ckeditor.css" />',
	entities: false,
	toolbarGroups: [
					 { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
					 { name: 'links' },
					 { name: 'insert' },
					 { name: 'tools' },
					 { name: 'others' },
					 <sec:authorize access="hasRole('ROLE_ADMIN')">{ name: 'document', groups: [ 'mode' ] },</sec:authorize>
					 '/',
					 { name: 'styles' },
					 { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					 { name: 'colors' },
					 { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
	                ]
});
</script>
