<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">login</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">login</c:param>
    </c:import>

    <sec:authorize access="isAuthenticated()">
        <c:redirect url="/index.jsp" />
    </sec:authorize>

    <div id="edit" class="container">

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <h4 class="card-header bg-dark text-white text-center">log in</h4>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="<c:url value="/oauth2/authorization/google" />"><img id="google" src="<c:url value='/img/btn_google_signin_dark_normal_web.png'/>" data-toggle="tooltip" title="<fmt:message key="subject.google.tooltip" />" /></a>
                            </div>
                        </div>

                        <div style="margin-top: 20px; margin-bottom: 20px; border: 0px; border-top: 1px solid #dddddd; text-align: center; height: 0px; line-height: 0px;">
                            <span style="background-color: #ffffff; padding: 0px 5px;"> or </span>
                        </div>

                        <form action="<c:url value="/subject/login" />" method="POST">
                            <sec:csrfInput />
                            <div class="form-group row">
                                <label for="username" class="col-form-label col-md-3 text-right">email</label>
                                <div class="col-md-9">
                                    <input type="text" id="username" name="username" class="form-control${fn:startsWith(message, 'subjects.login.') ? ' is-invalid' : '' }" placeholder="email address" data-toggle="tooltip"
                                        title="<fmt:message key="username.username.tooltip" />" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="passsword" class="col-form-label col-md-3 text-right">password</label>
                                <div class="col-md-9">
                                    <input type="password" id="password" name="password" class="form-control${fn:startsWith(message, 'subjects.login.') ? ' is-invalid' : '' }" placeholder="password" data-toggle="tooltip"
                                        title="<fmt:message key="login.password.tooltip" />" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-md-3 col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="rememberMe" id="rememberMe" />
                                        <label class="custom-control-label" for="rememberMe" data-toggle="tooltip" title="<fmt:message key="login.rememberMe.tooltip" />">stay logged in</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary float-right">log in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-right small">
                        <a href="<c:url value="/subject/forgot" />" data-toggle="tooltip" title="<fmt:message key="subject.password.tooltip" />"> forgot password </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <h4 class="card-header bg-dark text-white text-center">register</h4>
                    <div class="card-body">

                        <c:url var="action" value="/subject/save" />
                        <form:form modelAttribute="register" action="${action}" method="POST">
                            <form:hidden path="timeZone" />

                            <spring:bind path="username.username">
                                <div class="form-group row">
                                    <label for="username.username" class="col-form-label col-md-3 text-right">email</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="register.username.tooltip" />
                                        <form:input path="username.username" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="email address" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="username.username" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="password.password1">
                                <div class="form-group row">
                                    <label for="password.password1" class="col-form-label col-md-3 text-right">password</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="password.password1.tooltip" />
                                        <form:password path="password.password1" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="password" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="password.password1" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="password.password2">
                                <div class="form-group row">
                                    <label for="password.password2" class="col-form-label col-md-3 text-right">password again</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="password.password2.tooltip" />
                                        <form:password path="password.password2" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="password again" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="password.password2" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="name.givenName">
                                <div class="form-group row">
                                    <label for="name.givenName" class="col-form-label col-md-3 text-right">given name</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="name.givenName.tooltip" />
                                        <form:input path="name.givenName" class="form-control${status.error ? ' is-invalid' : '' }" placeholder="given name" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="name.givenName" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="name.familyName">
                                <div class="form-group row">
                                    <label for="name.familyName" class="col-form-label col-md-3 text-right">family name</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="name.familyName.tooltip" />
                                        <form:input path="name.familyName" class="form-control${status.error ? ' is-invalid' : '' }" placeholder="family name" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="name.familyName" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="answer">
                                <div class="form-group row">
                                    <form:hidden path="human" />
                                    <label for="answer" class="col-form-label col-md-3 text-right">${register.human.question}</label>
                                    <div class="col-md-9">
                                        <fmt:message var="tooltip" key="register.answer.tooltip" />
                                        <form:input path="answer" class="form-control${status.error ? ' is-invalid' : '' }" placeholder="${register.human.answer}" data-toggle="tooltip" title="${tooltip}" />
                                        <form:errors path="answer" cssClass="invalid-feedback" role="alert" element="div" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="termsAndConditions">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-6">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox cssClass="custom-control-input${status.error ? ' is-invalid' : '' }" path="termsAndConditions" />
                                            <label for="termsAndConditions1" class="custom-control-label" data-toggle="tooltip" title="<fmt:message key="register.termsAndConditions.tooltip" />">
                                                I agree to the <a href="https://tetsuwan-tech.com/about/terms-conditions/" target="_blank" style="text-decoration: underline;">Terms and Conditions</a>
                                            </label>
                                            <form:errors path="termsAndConditions" cssClass="invalid-feedback" role="alert" element="div" />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary float-right">register</button>
                                    </div>
                                </div>
                            </spring:bind>
                        </form:form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
					$("#google")
							.hover(
									function() {
										$(this)
												.prop('src',
														'<c:url value="/img/btn_google_signin_dark_focus_web.png" />');
									},
									function() {
										$(this)
												.prop('src',
														'<c:url value="/img/btn_google_signin_dark_normal_web.png" />');
									})
							.click(
									function() {
										$(this)
												.prop('src',
														'<c:url value="/img/btn_google_signin_dark_pressed_web.png" />');
									});

					$(document).ready(function() {
						$('#timeZone').val(moment.tz.guess());
					});
				</script>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
