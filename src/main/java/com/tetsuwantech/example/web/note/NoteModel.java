package com.tetsuwantech.example.web.note;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tetsuwantech.atom.web.DeleteModel;
import com.tetsuwantech.example.database.notebook.note.Note;
import com.tetsuwantech.example.web.Sequenceable;
import com.tetsuwantech.example.web.notebook.NotebookModel;
import com.tetsuwantech.example.web.search.Searchable;

public class NoteModel extends DeleteModel implements Sequenceable, Searchable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LONG_LENGTH)
	private String title;
	private String text;

	// handled by ajax
	private Long sequence;
	private NotebookModel notebook;

	public NoteModel() {
	}

	public NoteModel(Long id) {
		super(id);
	}

	public NoteModel(Long id, String title) {
		super(id);
		setTitle(title);
	}

	public NoteModel(Note note) {
		super(note);
		setTitle(note.getTitle());
		setText(note.getText());
		setSequence(note.getSequence());

		setNotebook(new NotebookModel(note.getNotebook().getId()));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public Long getSequence() {
		return sequence;
	}

	@Override
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public NotebookModel getNotebook() {
		return notebook;
	}

	public void setNotebook(NotebookModel notebook) {
		this.notebook = notebook;
	}

	// For lucene highlight
	private String searchDescription;
	private String searchPath;

	@Override
	public void setSearchPath(String searchPath) {
		this.searchPath = searchPath;
	}

	@Override
	public String getSearchPath() {
		return searchPath;
	}

	@Override
	public void setSearchDescription(String searchDescription) {
		this.searchDescription = searchDescription;
	}

	@Override
	public String getSearchDescription() {
		return searchDescription;
	}

	@Override
	public String getSearchURL() {
		return "/note/edit?notebook.id=" + getNotebook().getId() + "&id=" + getId();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof NoteModel)) {
			return false;
		}

		return true;
	}
}
