# Welcome

Welcome to the Example Application for the Atom Project

## Getting Started
Clone the repository with git

```console
git clone https://bitbucket.org/tetsuwantech/example.git
```

## Build dev environment
### Local configuration
There are several configuration items that are required to customise for your local development environment. These are kept in `~/.m2/settings.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings
        xmlns="http://maven.apache.org/SETTINGS/1.0.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="
                http://maven.apache.org/SETTINGS/1.0.0
                http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <profiles>
        <profile>
            <id>atom</id>
            <properties>
                <!--  OAuth 2.0 Client IDs from https://console.developers.google.com/apis/credentials -->
                <google.clientId></google.clientId>
                <google.clientSecret></google.clientSecret>

                <rememberMeAuthenticationProvider.key>my secret key</rememberMeAuthenticationProvider.key>

                <!-- path for storing indexes for search -->
                <hibernate.search>/tmp/index</hibernate.search>

                <!-- email details for atom -->
                <atom.from>Example Application &lt;example@example.com&gt;</atom.from>
                <!-- this is used as the test account for your login and Bcc on any email -->
                <atom.bcc>you@gmail.com</atom.bcc>

                <!-- your SMTP details for sending email -->
                <!-- See https://myaccount.google.com/apppasswords for Application Specific Password if you're using 2FA and get org.springframework.mail.MailAuthenticationException: Authentication failed; nested exception is javax.mail.AuthenticationFailedException: 534-5.7.9 Application-specific password required.  -->
                <mail.host>smtp.googlemail.com</mail.host>
                <mail.protocol>smtp</mail.protocol>
                <mail.port>587</mail.port>
                <mail.username>you@gmail.com</mail.username>
                <mail.password>your password</mail.password>
                <mail.smtp.auth>true</mail.smtp.auth>
                <mail.smtp.starttls.enable>true</mail.smtp.starttls.enable>
                
				<!-- this should not change -->
                <atom.timeZone>UTC</atom.timeZone>

            </properties>
        </profile>
    </profiles>
```

### Create local database
The code is database agnostic. The JDBC string is set in `pom.xml` 

We prefer **PostgreSQL**, the JDBC string is already set.

```console
createuser --username=postgres --password --pwprompt example
createdb --username=postgres --password --owner=example example
```

Or for **MySQL** 

```console
mysqladmin --user=root --password create example
mysql --user=root --password --execute="GRANT ALL PRIVILEGES ON example.* TO 'example'@'localhost' IDENTIFIED BY 'example';"
```

Change `pom.xml` to use (as required for the maven profile)

```xml
<jdbc.database>mysql</jdbc.database>
<jdbc.driverClass>com.mysql.jdbc.Driver</jdbc.driverClass>
<jdbc.url>jdbc:mysql://localhost:3306/example?useSSL=false&amp;amp;autoReconnect=true&amp;amp;characterEncoding=UTF-8&amp;amp;useLegacyDatetimeCode=false</jdbc.url>
<hibernate.import>import.mysql.sql</hibernate.import>
```
### Set up tomcat container
In order to run in development mode, Spring needs to run in a specific development profile (which is different to a maven profile). When running tomcat, include in the CATALINA_OPTS or VM Arguments

```
-Dspring.profiles.active=hibernate-auto
```

### Maven profiles
When compiling use maven profiles

* dev
* atom

```console
mvn --activate-profiles dev,atom clean package
```

### Running dev environment
If manually setting up tomcat, copy `target/example-4.0.0.RELEASE.war` to the `webapps` directory and restart tomcat.

Once running, go to [http://localhost:8080/example-4.0.0.RELEASE](http://localhost:8080/example-4.0.0.RELEASE) and log in with your email address set in **atom.bcc** and password **Example!**. These are stored in `src/main/resources/import.postgresql.sql`.
## Build uat environment
### Maven profiles
When compiling use maven profiles

* uat
* atom

```console
mvn --activate-profiles atom,uat clean package
```

### Liquibase configuration
As the uat environment uses liquibase for the database updates you will need to set up a database prior to running the code. From a running development environment export a copy of the database.

For **PostgreSQL**

```console
pg_dump --username=example --password example > src/main/resources/create.postgresql.sql 
```

And then recreate the database for uat, changing the session tables. If you're running this on the same machine, you will need to shutdown the development environment.

```console
dropdb --username=postgres --password example
createdb --username=postgres --password --owner=example example
psql --username=example --password example < src/main/resources/create.postgresql.sql 
psql --username=example --password example -c "alter table spring_session_attributes rename to subject_session_attributes;"
psql --username=example --password example -c "alter table spring_session rename to subject_session;"
```
For **MySQL**

```console
mysqldump --set-gtid-purged=OFF -u example -p example > src/main/resources/create.mysql.sql
```

And create the database, then modify the session tables with

```console
mysqladmin --user=root --password drop example
mysqladmin --user=root --password create example
mysql --user=example --password example < src/main/resources/create.mysql.sql
mysql --user=example --password --execute='alter table SPRING_SESSION_ATTRIBUTES rename SUBJECT_SESSION_ATTRIBUTES;' example
mysql --user=example --password --execute='alter table SPRING_SESSION rename SUBJECT_SESSION;' example
mysql --user=root --password --execute="SET GLOBAL time_zone = 'UTC';"
```

Otherwise if you have a running production database from a previous version, use that.
### tomcat configuration
Once built, set up tomcat (this is from the `example/` directory). Your account will need to be in the `tomcat8` group.

```console
sudo systemctl stop tomcat8
sudo rm -fr /var/lib/tomcat8/webapps/example* /tmp/index
sudo cp target/example.war /var/lib/tomcat8/webapps/
sudo chown -R tomcat8.tomcat8 /var/lib/tomcat8/webapps/
sudo systemctl start tomcat8
```

And then tail the output of `catalina.out`

```console
tail -F /var/log/tomcat8/catalina.out
```

## Build prod environment
Left as an exercise to the reader.
