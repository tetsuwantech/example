package com.tetsuwantech.example.manager.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.search.highlight.DefaultEncoder;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.Scorer;
import org.apache.lucene.search.highlight.TextFragment;

// https://issues.apache.org/jira/browse/LUCENE-6471
public class HibernateHighlighter extends Highlighter {

	public HibernateHighlighter(Formatter formatter, Scorer fragmentScorer) {
		super(formatter, new DefaultEncoder(), fragmentScorer);
	}

	public final String getBasicTextFragments(String result, int maxLength, String separator) {
		return StringUtils.isEmpty(result) ? "" : result.substring(0, Math.min(result.length(), maxLength)) + (result.length() > maxLength ? separator : "");
	}

	public final String getBetterTextFragments(TokenStream tokenStream, String text, int maxNumFragments, String separator) throws IOException, InvalidTokenOffsetsException {

		maxNumFragments = Math.max(1, maxNumFragments); // sanity check
		TextFragment[] frag = getBestTextFragments(tokenStream, text, false, maxNumFragments);

		StringBuilder result = new StringBuilder();
		for (int i = 0; i < frag.length; i++) {
			if (frag[i] != null && frag[i].getScore() > 0) {
				if (i > 0) {
					result.append(separator);
				}
				result.append(frag[i].toString());
			}
		}
		return result.toString();

	}

}
