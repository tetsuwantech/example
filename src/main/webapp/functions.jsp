<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />
        
<c:set var="config" value="${sessionScope.config}" />
        
<script type="text/javascript">
	var isDirty = false;
	var $throbber = $('<div>').attr({
		id : 'throbber'
	}).hide();

	// TODO: https://stackoverflow.com/questions/14561988/handling-ctrls-keypress-event-for-browser
	$(document).ready(function() {

		$($("input:text:visible").not('[disabled]')[1]).focus();

		var url = document.location.toString();
		if (url.match('#'))
			$('.nav-tabs a[href*="#' + url.split('#')[1] + '"]').tab('show');

		<c:choose>
		<c:when test="${empty config.useToolTips or config.useToolTips}">
		$('[data-toggle=tooltip]').tooltip({
			container : 'body',
			trigger : 'hover'
		});
		</c:when>
		<c:otherwise>
		$('[data-toggle=tooltip]').removeAttr('title');
		</c:otherwise>
		</c:choose>

		$('#sortable').sortable({
			containment : '#sortable',
			cursor : 'move',
			revert : 'invalid',
			placeholder: 'bg-light',
			start : function(event, ui) {
				ui.placeholder.height(ui.item.height())
				ui.placeholder.width(ui.item.width())
			},
			update : function(event, ui) {
				$('body').prepend($throbber.show());

				var data = [], i = 1;
				$(ui.item.parent()).find('div[data-type]').each(function() {
					if ($(this).find('[name*=sequence]').attr('value') != i) {
						data.push({
							'id' : $(this).find('[name=id]').attr('value'),
							'sequence' : i
						});
						$(this).find('[name*=sequence]').attr('value', i)
					}
					i++;
				})

				doAction($('#reorder').prop('action'), data);
			},
		});

		$('#edit').find('form').change(function() {
			isDirty = true;
		});

		$('[type!=hidden][name$=Date]').on('change.datetimepicker', function(event) {
			isDirty = true;
		});

		$('#edit').find(':submit').on('click', function() {
			isDirty = false;
			if (typeof CKEDITOR.instances.editor !== 'undefined') {
				CKEDITOR.instances.editor.resetDirty();
			}
			$('body').prepend($throbber.show());
		});

		$('a[href*=remove]').on('click', function() {

			$('body').prepend($throbber.show());

			if (!window.confirm('Are you sure you want to do this?')) {
				$throbber.hide();
				return false;
			}

			isDirty = false;
			if (typeof CKEDITOR.instances.editor !== 'undefined') {
				CKEDITOR.instances.editor.resetDirty();
			}

			return true;
		});

	});

	function unLoad() {

		var isLoginPage = $('#google').length > 0;
		if (!isLoginPage && (isDirty || (typeof CKEDITOR.instances.editor !== 'undefined' && CKEDITOR.instances.editor.checkDirty()))) {

			$(window).off('beforeunload');
			timeout = setTimeout(function() {
				$throbber.hide();
				$(window).on('beforeunload', unLoad);
			}, 100);
			return 'You have unsaved changes.';
		}
	}

	$(window).on('beforeunload', unLoad);

	var actions = {

		remove : function(data, result) {

			var type = data.type, id = data.id;

			$('[data-toggle=tooltip]').tooltip('hide');
			$('[data-type=' + type + '][data-id=' + id + ']').remove();

		},

	}

	function doMessage(message, type, icon) {

		var type = typeof type === 'undefined' ? 'success' : type;
		var icon = typeof icon === 'undefined' ? 'check-circle' : icon;

		$('[data-type=message]').hide();

		if (!$('[data-type=ajax-message-content]').length) {

			$('[data-type=ajax-message]').append(
					$('<div>').addClass('alert alert-' + type + ' alert-dismissible fade show').attr({
						'data-type' : 'ajax-message-content',
						role : 'alert'
					}));

		}

		$('[data-type=ajax-message-content]')
		.removeClass('alert-success alert-info alert-warning alert-danger')
		.addClass('alert-' + type).html($('<button>').addClass('close')
		.attr({
			'aria-label' : 'close'
		}).append($('<span>').attr({
			'aria-hidden' : 'true'
		})).on('click', function() {
			$(this).alert('close');
		})).append($('<span>').addClass('fas fa-' + icon).attr({
			'aria-hidden' : 'true'
		})).append(' ' + message)
	}

	function doAction(url, content, callback, throbber) {

		var method = typeof content.method === 'undefined' ? 'POST' : content.method;

		if (typeof throbber !== 'undefined' && throbber)
			$('body').prepend($throbber.show());

		if (url.indexOf('remove') > 0 && !window.confirm('Are you sure you want to remove this?')) {
			$throbber.hide();
			return false;
		}

		$.ajax({
			url : url,
			method : method,
			data : method == 'GET' ? null : JSON.stringify(content),
			dataType : 'json',
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'X-CSRF-TOKEN' : '${_csrf.token}'
			},
			success : function(data, textStatus, xhr) {

				if (typeof callback !== 'undefined')
					callback(content, data);

				$throbber.hide();

				if (typeof data.message !== 'undefined' && data.message != 'false')
					doMessage(data.message);

			},
			error : function(xhr, textStatus, errorThrown) {
				if (xhr.status == 403 || textStatus == 'parsererror') {
					if (xhr.responseText.match('rememberMe') != null) {
						alert('<fmt:message key="subjects.logout.invalid" />');
						window.location.assign('<c:url value="/subject?status=logout.invalid&type=danger" />');
					} else if (xhr.responseText.match('http-equiv="refresh"')) {
						alert('<fmt:message key="subjects.logout.timeout" />');
						window.location.reload(true);
					}
				} else
					alert('Something went wrong. ' + xhr.status + ': ' + errorThrown);

				$throbber.hide();
			}
		})
	}
</script>
