package com.tetsuwantech.example.database.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;
import com.tetsuwantech.atom.database.authentication.Securable;
import com.tetsuwantech.atom.database.authentication.Subject;

@Entity
@Cacheable
@Table(name = "config")
public class Config extends GenericEntity implements Securable {

	private static final long serialVersionUID = 1L;

	// do no updates to subject
	@Auditable
	@OneToOne(targetEntity = Subject.class, cascade = CascadeType.MERGE)
	@JoinColumn(name = "subject_id", unique = true, updatable = false)
	@NotNull
	private Subject subject;

	@Auditable
	@Column(name = "use_tool_tips")
	private Boolean useToolTips = true;

	@Auditable
	@Column(name = "set_profile")
	private Boolean setProfile = true;

	@Auditable
	@Column(name = "show_motd")
	private Boolean showMotd = true;

	@Auditable
	@Column(name = "receive_email")
	private Boolean receiveEmail = true;

	@Override
	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * @return the useTooltips
	 */
	public Boolean getUseToolTips() {
		return useToolTips;
	}

	/**
	 * @param useTooltips
	 *            the useTooltips to set
	 */
	public void setUseToolTips(Boolean useToolTips) {
		this.useToolTips = useToolTips;
	}

	/**
	 * @return the setProfile
	 */
	public Boolean getSetProfile() {
		return setProfile;
	}

	/**
	 * @param setProfile
	 *            the setProfile to set
	 */
	public void setSetProfile(Boolean setProfile) {
		this.setProfile = setProfile;
	}

	/**
	 * @return the motd
	 */
	public Boolean getShowMotd() {
		return showMotd;
	}

	/**
	 * @param showMotd
	 *            the motd to set
	 */
	public void setShowMotd(Boolean showMotd) {
		this.showMotd = showMotd;
	}

	/**
	 * @return the receiveEmail
	 */
	public Boolean getReceiveEmail() {
		return receiveEmail;
	}

	/**
	 * @param receiveEmail
	 *            the receiveEmail to set
	 */
	public void setReceiveEmail(Boolean receiveEmail) {
		this.receiveEmail = receiveEmail;
	}

}
