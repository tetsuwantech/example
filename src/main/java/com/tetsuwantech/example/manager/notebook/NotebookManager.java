package com.tetsuwantech.example.manager.notebook;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.config.ConfigService;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.database.notebook.NotebookService;
import com.tetsuwantech.example.web.Sequenceable;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.notebook.NotebookModel;

@Named(value = "NotebookManager")
public class NotebookManager extends GenericManager<Notebook, Config, NotebookModel, ConfigModel> {

	@Inject
	private NotebookService notebookService;

	@Inject
	private SubjectService subjectService;

	@Inject
	private ConfigService configService;

	@Override
	public NotebookModel createModel(Notebook notebook) {
		return new NotebookModel(notebook);
	}

	@Override
	public void save(NotebookModel notebookModel) {

		// if new, create a notebook, otherwise populate the new bits
		Notebook notebook;
		boolean isNew = false;
		if (notebookModel.getId() == null) {

			notebook = new Notebook();
			Config config = configService.get(notebookModel.getConfig().getId());
			notebook.setConfig(config);

			notebook.setSequence(notebookService.count(config) + 1L);

			isNew = true;

		} else {
			notebook = notebookService.get(notebookModel.getId());
		}

		// set the data that's updated
		notebook.setTitle(notebookModel.getTitle());
		notebook.setDescription(notebookModel.getDescription());

		// reset this, as it might have been a new save
		if (isNew) {
			notebookService.create(notebook);
			notebookModel.setId(notebook.getId());
		} else {
			notebookService.save(notebook);
		}
	}

	@Override
	public List<NotebookModel> findAll(ConfigModel configModel) {
		Config config = configService.get(configModel.getId());
		return super.findAll(config, null, "sequence");
	}

	public Long count(ConfigModel configModel) {
		Config config = configService.get(configModel.getId());
		return super.count(config, null);
	}

	@Override
	public Long countAll(SubjectModel subjectModel) {
		Subject subject = subjectService.get(subjectModel.getId());
		Config config = configService.findFirst(subject);

		// this doesn't need to be totaled up
		return super.count(config, null);
	}

	@Override
	public List<NotebookModel> search(ConfigModel configModel, String text) {
		Config config = configService.get(configModel.getId());
		return super.search(config, null, text, new String[] { "title", "description" });
	}

	public void updateSequence(NotebookModel model) {
		Notebook entity = notebookService.get(model.getId());
		((com.tetsuwantech.example.database.Sequenceable) entity).setSequence(((Sequenceable) model).getSequence());
		notebookService.save(entity);
	}

}
