<%--
  #%L
  example
  %%
  Copyright (C) 2018 Tetsuwan Technology
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<fmt:setLocale value="en_AU" />
<fmt:setBundle basename="example-messages" />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/head.jsp">
    <c:param name="title">change password</c:param>
</c:import>
<body>
    <c:import url="/menu.jsp">
        <c:param name="menu">config</c:param>
    </c:import>

    <sec:authorize access="isAuthenticated()">
        <c:redirect url="/index.jsp" />
    </sec:authorize>
 
    <div id="edit" class="container">
        <div class="card">
            <h4 class="card-header bg-dark text-white text-center">change password</h4>
            <div class="card-body">

                <c:url var="action" value="/subject/password" />
                <form:form modelAttribute="password" cssClass="form-horizontal" action="${action}" method="POST">
                    <input type="hidden" name="uuid" value="${uuid.uuid}" />

                    <spring:bind path="password1">
                        <div class="form-group row">
                            <label for="password1" class="col-form-label col-md-3 text-right">password</label>
                            <div class="col-md-9">
                                <fmt:message var="tooltip" key="password.password1.tooltip" />
                                <form:password path="password1" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="password" data-toggle="tooltip" title="${tooltip}" />
                                <form:errors path="password1" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="password2">
                        <div class="form-group row">
                            <label for="password2" class="col-form-label col-md-3 text-right">password again</label>
                            <div class="col-md-9">
                                <fmt:message var="tooltip" key="password.password2.tooltip" />
                                <form:password path="password2" cssClass="form-control${status.error ? ' is-invalid' : '' }" placeholder="password again" data-toggle="tooltip" title="${tooltip}" />
                                <form:errors path="password2" cssClass="invalid-feedback" role="alert" element="div" />
                            </div>
                        </div>
                    </spring:bind>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary float-right">change</button>
                        </div>
                    </div>

                </form:form>

            </div>
        </div>
    </div>

    <c:import url="/functions.jsp" />
    <c:import url="/footer.jsp" />

</body>
</html>
