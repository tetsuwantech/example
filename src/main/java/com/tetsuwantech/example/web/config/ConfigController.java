package com.tetsuwantech.example.web.config;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.MailSender;
import com.tetsuwantech.atom.util.MailSender.Message;
import com.tetsuwantech.atom.util.TimeZoneUtil;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.manager.config.ConfigManager;

@Controller
@SessionAttributes(value = "config")
public class ConfigController {

	// private static Logger LOG = LoggerFactory.getLogger(ConfigController.class);

	@Inject
	private ConfigManager configManager;

	@Inject
	private SubjectManager subjectManager;

	@Inject
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Inject
	private SubjectHandler subjectHandler;

	@Inject
	private MailSender mailSender;

	@InitBinder(value = "config")
	private void initBinder(WebDataBinder binder) {
		binder.addValidators(new ConfigModelValidator(bCryptPasswordEncoder, subjectHandler));
	}

	@ModelAttribute
	public void populateModel(@SessionAttribute(name = "config") ConfigModel config, Model model) {

		config = configManager.get(config.getId());
		config.setSubject(SubjectUtils.getSubject());
		boolean isDeactivatable = subjectHandler.isDeactivatable(config.getSubject());
		config.getSubject().setDeactivatable(isDeactivatable);

		model.addAttribute("config", config);
		model.addAttribute("timeZones", TimeZoneUtil.getAllTimeZones());
	}

	@GetMapping(value = "/profile")
	public String edit(@SessionAttribute(name = "config") ConfigModel config, @RequestParam(required = false) String source, Model model) {

		String type = "info", message = "config.edit.edit";

		// first time and profile hasn't been saved
		if (config.getSetProfile()) {
			message = "config.edit.setProfile";
			type = "warning";

			// this is (probably) from social login - set in spring-context.xml
		} else if (StringUtils.isNotEmpty(source)) {

			// check is password is null
			SubjectModel subject = SubjectUtils.getSubject();
			message = subject.getPassword() == null ? "oauth2.login.disconnectPassword" : "oauth2.login.disconnect";
			type = subject.getPassword() == null ? "danger" : "success";
		}

		model.addAttribute("message", message);
		model.addAttribute("type", type);
		return "/configure/edit";
	}

	@PostMapping(value = "/profile/save")
	public String save(HttpServletRequest request, @Valid @ModelAttribute(name = "config") ConfigModel config, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("message", "errors");
			model.addAttribute("type", "danger");
		} else {
			config.setSetProfile(false);
			configManager.save(config);

			// manage Subject - to make sure it's saved in the authenticated object
			SubjectModel subject = SubjectUtils.getSubject();
			subject.setFamilyName(config.getName().getFamilyName());
			subject.setGivenName(config.getName().getGivenName());
			subject.setTimeZone(config.getSubject().getTimeZone());
			if (StringUtils.isNotEmpty(config.getChangePassword().getPassword().getPassword1())) {
				subject.setPassword(config.getChangePassword().getPassword().getPassword1());
			}

			// check for deactivate
			if (config.getDeactivate()) {

				try {
					// set UUID for this action
					subject.setUuid(UUID.randomUUID().toString());

					Map<String, Object> content = new TreeMap<>();
					content.put("subject", subject);

					String url = new URL(request.getScheme(), request.getServerName(), request.getServerPort() == 80 || request.getServerPort() == 443 ? -1 : request.getServerPort(),
							request.getContextPath() + "/subject/deactivate?uuid=" + subject.getUuid()).toExternalForm();
					content.put("url", url);
					mailSender.send(subject, Message.DEACTIVATE_ACCOUNT, content);

				} catch (MalformedURLException exception) {
					exception.printStackTrace();
				}

			}

			subjectManager.save(subject);

			// re-set the current user so password, etc are valid
			SubjectUtils.login(subject);

			populateModel(config, model);
			model.addAttribute("message", "config.edit.save");
		}

		return "/configure/edit";
	}

	// this is used by SwitchUserFilter in spring-context.xml
	@GetMapping(value = "/profile/reset")
	public String reset(HttpSession httpSession, @RequestParam(required = false) String source, Model model) {

		SubjectModel subject = SubjectUtils.getSubject();
		ConfigModel config = configManager.findFirst(subject);
		config.setSubject(subject);

		// this re-sets the session value of config, need to force this
		httpSession.setAttribute("config", config);

		model.asMap().clear();
		return "redirect:/profile" + Optional.ofNullable(source).map(s -> "?source=" + s).orElse("");
	}

}
