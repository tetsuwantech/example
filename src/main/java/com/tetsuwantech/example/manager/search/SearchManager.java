package com.tetsuwantech.example.manager.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.hibernate.search.exception.EmptyQueryException;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.example.database.config.Config;
import com.tetsuwantech.example.database.config.ConfigService;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.database.notebook.note.Note;
import com.tetsuwantech.example.database.search.Search;
import com.tetsuwantech.example.database.search.SearchService;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.note.NoteModel;
import com.tetsuwantech.example.web.notebook.NotebookModel;
import com.tetsuwantech.example.web.search.SearchModel;
import com.tetsuwantech.example.web.search.SearchModel.Filters;

@Named(value = "SearchManager")
public class SearchManager {

	@Inject
	private SearchService searchService;

	@Inject
	private ConfigService configService;

	private static Pattern HTMLPattern = Pattern.compile("<[^>]+?>");
	private static Pattern NewLinePattern = Pattern.compile("\n|\r");

	public List<? extends GenericModel> search(ConfigModel configModel, SearchModel searchModel) throws EmptyQueryException {

		List<GenericModel> models = new LinkedList<>();
		Search search = new Search();

		// this sets up paginations
		search.setFirstResult(searchModel.getPageSize() * (searchModel.getPageNumber() - 1));
		search.setMaxResults(searchModel.getPageSize());

		// if no filters, then all the filters
		search.setClasses((searchModel.getFilters().isEmpty() ? Stream.of(Filters.values()) : searchModel.getFilters().stream()).map(Filters::getField).collect(Collectors.toList()));
		search.setText(searchModel.getText());

		Config config = configService.get(configModel.getId());
		List<? extends GenericEntity> entities = searchService.search(config, search);

		// for display
		searchModel.setResultSize(search.getResultSize());
		searchModel.setTotalPages(1 + searchModel.getResultSize() / searchModel.getPageSize());

		// set up the lucene highlighter
		QueryScorer queryScorer = new QueryScorer(search.getQuery());
		HibernateHighlighter highlighter = new HibernateHighlighter(new SimpleHTMLFormatter(Search.HIGHLIGHT_OPEN, Search.HIGHLIGHT_CLOSE), queryScorer);
		highlighter.setTextFragmenter(new SimpleSpanFragmenter(queryScorer, Search.FRAGMENT_SIZE));
		Analyzer analyzer = new StandardAnalyzer();

		for (GenericEntity entity : entities) {

			if (entity instanceof Notebook) {
				Notebook notebook = (Notebook) entity;
				NotebookModel model = new NotebookModel(notebook.getId(), notebook.getTitle());

				String title = highlighter.getBasicTextFragments(notebook.getTitle(), Search.LONG_FRAGMENT_SIZE, Search.ELLIPSIS);
				String description = highlighter.getBasicTextFragments(notebook.getDescription(), Search.LONG_FRAGMENT_SIZE, Search.ELLIPSIS);

				try {
					if (StringUtils.isNotEmpty(searchModel.getText())) {
						title = highlighter.getBetterTextFragments(analyzer.tokenStream("title", notebook.getTitle()), notebook.getTitle(), Search.MAX_NUMBER_OF_FRAGMENTS, Search.ELLIPSIS);
						description = StringUtils.isEmpty(notebook.getDescription()) ? ""
								: highlighter.getBetterTextFragments(analyzer.tokenStream("description", notebook.getDescription()), notebook.getDescription(), Search.MAX_NUMBER_OF_FRAGMENTS, Search.ELLIPSIS);
					} else {
						description = highlighter.getBasicTextFragments(description, Search.LONG_FRAGMENT_SIZE, Search.ELLIPSIS);
					}
				} catch (IOException | InvalidTokenOffsetsException e) {
					e.printStackTrace();
				}

				model.setSearchDescription(title + (StringUtils.isEmpty(title) || StringUtils.isEmpty(description) ? "" : Search.ELLIPSIS) + description);
				model.setSearchPath(notebook.getTitle());
				models.add(model);
			} else if (entity instanceof Note) {
				Note note = (Note) entity;
				Notebook notebook = note.getNotebook();
				NoteModel model = new NoteModel(note.getId(), note.getTitle());
				model.setNotebook(new NotebookModel(notebook.getId()));

				String title = highlighter.getBasicTextFragments(note.getTitle(), Search.LONG_FRAGMENT_SIZE, Search.ELLIPSIS);
				String description = HTMLPattern.matcher(StringUtils.isEmpty(note.getText()) ? "" : note.getText()).replaceAll(" ");
				description = NewLinePattern.matcher(description).replaceAll(" ");

				// title is skipped above when building description
				try {
					if (StringUtils.isNotEmpty(searchModel.getText())) {
						title = highlighter.getBetterTextFragments(analyzer.tokenStream("title", note.getTitle()), note.getTitle(), Search.MAX_NUMBER_OF_FRAGMENTS, Search.ELLIPSIS);
						description = highlighter.getBetterTextFragments(analyzer.tokenStream("text", description), description, Search.MAX_NUMBER_OF_FRAGMENTS, Search.ELLIPSIS);
					} else {
						description = highlighter.getBasicTextFragments(description, Search.LONG_FRAGMENT_SIZE, Search.ELLIPSIS);
					}
				} catch (IOException | InvalidTokenOffsetsException e) {
					e.printStackTrace();
				}

				model.setSearchDescription(title + (StringUtils.isEmpty(title) || StringUtils.isEmpty(description) ? "" : Search.ELLIPSIS) + description);
				model.setSearchPath(notebook.getTitle() + " > " + note.getTitle());
				models.add(model);
			}
		}

		analyzer.close();

		// return list of forms with search text highlighted
		return models;
	}

	public void rebuild() {

		Search search = new Search();
		search.setClasses(Stream.of(Filters.values()).map(Filters::getField).collect(Collectors.toList()));

		searchService.rebuild(search);
	}

}
