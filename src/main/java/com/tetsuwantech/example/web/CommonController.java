package com.tetsuwantech.example.web;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.example.manager.motd.MotdManager;
import com.tetsuwantech.example.manager.notebook.NotebookManager;
import com.tetsuwantech.example.web.motd.MotdModel;
import com.tetsuwantech.example.web.notebook.NotebookModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

@ControllerAdvice
@SessionAttributes(value = "config")
public class CommonController {

	// private static Logger LOG = LoggerFactory.getLogger(CommonController.class);

	@Inject
	private NotebookManager notebookManager;

	@Inject
	private MotdManager motdManager;

	@ModelAttribute
	public void populateModel(@RequestParam(required = false, name = "notebook.id") Long notebookId, Model model) {

		if (notebookId != null) {
			NotebookModel notebook = notebookManager.get(notebookId);
			model.addAttribute("notebook", notebook);
		}

		List<MotdModel> motds = motdManager.findAll();
		int i;
		for (i = 0; i < motds.size(); i++) {
			MotdModel motd = motds.get(i);
			if (DateTimePeriodUtils.isBetween(motd)) {
				break;
			}
		}

		// this can't be motd as it clashes with the motd model editor
		model.addAttribute("messageOfTheDay", i == motds.size() ? "" : motds.get(i).getDescription());

	}
}
