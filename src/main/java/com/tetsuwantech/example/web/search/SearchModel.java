package com.tetsuwantech.example.web.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.example.database.notebook.Notebook;
import com.tetsuwantech.example.database.notebook.note.Note;

public class SearchModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@Size(max = LONG_LENGTH)
	private String text;

	private int pageNumber = 1;
	private int pageSize = 10;
	private int totalPages;
	private int resultSize;

	public enum Filters {
		NOTEBOOK("notebooks", Notebook.class),
		NOTE("notes", Note.class);

		private String name;
		private Class<?> field;

		Filters(String name, Class<?> field) {
			this.name = name;
			this.field = field;
		}

		/**
		 * @return the field
		 */
		public Class<?> getField() {
			return this.field;
		}

		/**
		 * @param field
		 *            the field to set
		 */
		public void setField(Class<?> field) {
			this.field = field;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	private final Set<Filters> filters = new LinkedHashSet<>();

	public SearchModel() {
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

	/**
	 * @return the fields
	 */
	public Set<Filters> getFilters() {
		return this.filters;
	}

	/**
	 * @param filters
	 *            the fields to set
	 */
	public void setFilters(Set<Filters> filters) {
		this.filters.clear();
		this.filters.addAll(filters);
	}

}
