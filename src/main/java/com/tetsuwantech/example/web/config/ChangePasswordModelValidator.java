package com.tetsuwantech.example.web.config;

import org.apache.commons.lang3.StringUtils;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.PasswordModelValidator;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

public class ChangePasswordModelValidator implements Validator {

	// private static Logger LOG =
	// LoggerFactory.getLogger(ChangePasswordModelValidator.class);

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final Validator passwordModelValidator;

	@Override
	public boolean supports(Class<?> target) {
		return ChangePasswordModel.class.equals(target);
	}

	public ChangePasswordModelValidator(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.passwordModelValidator = new PasswordModelValidator();
	}

	@Override
	public void validate(Object object, Errors errors) {

		ChangePasswordModel changePassword = (ChangePasswordModel) object;

		SubjectModel subject = SubjectUtils.getSubject();

		// if we are logged in socially, then we can't change the password
		if (subject.isSocial()) {
			return;
		}

		boolean checkPassword = false;
		// if we have a password in subject, and new passwords are supplied we need to
		// check old matches
		if (subject.getPassword() != null && (StringUtils.isNotEmpty(changePassword.getPassword().getPassword1()) || StringUtils.isNotEmpty(changePassword.getPassword().getPassword2()))) {

			// check old password matches
			if (!bCryptPasswordEncoder.matches(changePassword.getOldPassword(), subject.getPassword())) {
				errors.rejectValue("oldPassword", "Password.wrongPassword");
			}

			checkPassword = true;
		}

		// new passwords fields have been provided
		if (changePassword.getPassword().getPassword1() != null && changePassword.getPassword().getPassword2() != null) {

			// password needs to be set if it's disconnected from social (so fields are
			// non-null) - passwordValidator will check they are the same
			if (subject.getPassword() == null && (StringUtils.isEmpty(changePassword.getPassword().getPassword1()) || StringUtils.isEmpty(changePassword.getPassword().getPassword2()))) {
				errors.rejectValue("password.password1", "Password.notNull");
			} else {
				checkPassword = true;
			}
		}

		// check if we're changing old password, or if current password is null - which
		// means we're doing a social update
		if (checkPassword) {
			errors.pushNestedPath("password");
			ValidationUtils.invokeValidator(this.passwordModelValidator, changePassword.getPassword(), errors);
			errors.popNestedPath();
		}

	}
}
