package com.tetsuwantech.example.database.search;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.hibernate.search.exception.EmptyQueryException;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.example.database.config.Config;

@Transactional
@Named(value = "SearchService")
public class SearchServiceImpl implements SearchService {

	@Inject
	SearchDAO searchDAO;

	@Override
	public List<? extends GenericEntity> search(Config config, Search search) {

		List<? extends GenericEntity> results;

		// If you only use stop words, catch it and return empty list
		try {
			results = searchDAO.search(config, search);
		} catch (EmptyQueryException exception) {
			results = new LinkedList<>();
		}
		return results;
	}

	@Override
	public void rebuild(Search search) {
		searchDAO.rebuild(search);
	}

}
