package com.tetsuwantech.example.web.note;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tetsuwantech.example.manager.notebook.note.NoteManager;
import com.tetsuwantech.example.web.ControllerHelper;
import com.tetsuwantech.example.web.ControllerHelper.Mapping;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.notebook.NotebookModel;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes(value = "config")
public class NoteController {

	// private static Logger LOG = LoggerFactory.getLogger(NoteController.class);

	@Inject
	private NoteManager noteManager;

	@Inject
	private MessageSource messageSource;

	private ControllerHelper<NoteManager, NoteModel, ?> controllerHelper;

	@PostConstruct
	private void init() {
		controllerHelper = new ControllerHelper<>(noteManager, Mapping.NOTE, messageSource);
	}

	@ModelAttribute
	public void populateModel(@RequestParam(name = "notebook.id") Long notebookId, @ModelAttribute(name = "note") NoteModel note, Model model) {

		if (note.getId() != null) {

			note = noteManager.get(note.getId());
			model.addAttribute("note", note);
			model.addAttribute("args", new String[] { note.getTitle() });

		} else if (note.getId() == null) {

			List<NoteModel> notes = noteManager.findAll(new NotebookModel(notebookId));
			model.addAttribute("notes", notes);
		}

	}

	// we don't force method = RequestMethod.GET here because it might be getting a
	// 403 redirect with status
	@GetMapping(value = "/note")
	public String main(Model model) {
		return "/notes/main";
	}

	@GetMapping(value = "/note/create")
	public String create(Model model) {
		model.addAttribute("message", "notes.edit.create");
		model.addAttribute("type", "info");
		return "/notes/edit";

	}

	@GetMapping(value = "/note/edit")
	public String edit(Model model) {
		// this will confirm we have permission to edit this
		model.addAttribute("message", "notes.edit.edit");
		model.addAttribute("type", "info");
		return "/notes/edit";
	}

	@DeleteMapping(value = "/api/note/remove")
	public @ResponseBody String remove(@SessionAttribute(name = "config") ConfigModel config, @RequestBody NoteModel note, Model model) {
		return controllerHelper.remove(config, note);
	}

	@PostMapping(value = "/api/note/reorder")
	public @ResponseBody String reorder(@RequestBody List<NoteModel> notes, Model model) {
		return controllerHelper.reorder(notes);
	}

	@GetMapping(value = "/note/copy")
	public String copy(@Valid @ModelAttribute(name = "note") NoteModel note, Model model) {

		// blank out id
		note.setId(null);
		note.setTitle("Copy of " + note.getTitle());

		model.addAttribute("message", "notes.edit.copy");
		return "/notes/edit";
	}

	@PostMapping(value = "/note/save")
	public String save(@SessionAttribute(name = "config") ConfigModel config, @RequestParam(name = "notebook.id") Long notebookId, @Valid @ModelAttribute(name = "note") NoteModel note, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("message", "errors");
			model.addAttribute("type", "danger");
		} else {

			noteManager.save(note);
			populateModel(notebookId, note, model);

			model.addAttribute("message", "notes.edit.save");
		}

		return "/notes/edit";
	}

}
