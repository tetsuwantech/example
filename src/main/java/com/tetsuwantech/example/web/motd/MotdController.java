package com.tetsuwantech.example.web.motd;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tetsuwantech.atom.util.TimeZoneUtil;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.manager.config.ConfigManager;
import com.tetsuwantech.example.manager.motd.MotdManager;
import com.tetsuwantech.example.web.ControllerHelper;
import com.tetsuwantech.example.web.ControllerHelper.Mapping;
import com.tetsuwantech.example.web.config.ConfigModel;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes(value = "config")
public class MotdController {

	// private static Logger LOG = LoggerFactory.getLogger(MotdController.class);

	@Inject
	private MotdManager motdManager;

	@Inject
	private ConfigManager configManager;

	@Inject
	private MessageSource messageSource;

	private ControllerHelper<MotdManager, MotdModel, ?> controllerHelper;

	@PostConstruct
	private void init() {
		controllerHelper = new ControllerHelper<>(motdManager, Mapping.MOTD, messageSource);
	}

	@InitBinder(value = "motd")
	private void initBinder(WebDataBinder binder) {
		binder.addValidators(new MotdModelValidator());
	}

	@ModelAttribute
	public void populateModel(@ModelAttribute(name = "motd") MotdModel motd, Model model) {

		if (motd.getId() != null) {
			// grab the data
			motd = motdManager.get(motd.getId());
			model.addAttribute("motd", motd);
			model.addAttribute("args", new String[] { motd.getTitle() });

		} else if (motd.getId() == null) {
			List<MotdModel> motds = motdManager.findAll();
			model.addAttribute("motds", motds);
		}
	}

	@GetMapping(value = "/admin/motd")
	public String main(Model model) {
		return "/motds/main";
	}

	@GetMapping(value = "/admin/motd/create")
	public String create(Model model) {

		// need to add new MotdModel as it will be still stuck from session
		model.addAttribute("motd", new MotdModel());

		model.addAttribute("message", "motds.edit.create");
		model.addAttribute("type", "info");
		return "/motds/edit";
	}

	@GetMapping(value = "/admin/motd/edit")
	public String edit(@ModelAttribute(name = "motd") MotdModel motd, Model model) {

		// a MotdEntity will always be defined - they cannot be created here
		model.addAttribute("message", "motds.edit.edit");
		model.addAttribute("type", "info");
		return "/motds/edit";
	}

	@DeleteMapping(value = "/admin/api/motd/remove")
	public @ResponseBody String remove(@SessionAttribute(name = "config") ConfigModel config, @RequestBody MotdModel motd, Model model) {
		return controllerHelper.remove(config, motd);
	}

	@PostMapping(value = "/admin/motd/save")
	public String save(@SessionAttribute(name = "config") ConfigModel config, @Valid @ModelAttribute(name = "motd") MotdModel motd, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("message", "errors");
			model.addAttribute("type", "danger");
		} else {

			// need to convert time times in the model to times of the server
			SubjectModel subject = SubjectUtils.getSubject();
			motd.setStartDate(TimeZoneUtil.toServerTimeZone(motd.getStartDate(), subject.getTimeZoneId()));

			if (motd.getEndDate() != null) {
				motd.setEndDate(TimeZoneUtil.toServerTimeZone(motd.getEndDate(), subject.getTimeZoneId()));
			}

			motdManager.save(motd);
			populateModel(motd, model);

			// need to reset ourself as well
			if (motd.getResetAllShowMotd()) {
				config.setShowMotd(true);
			}

			model.addAttribute("message", "motds.edit.save");
		}

		return "/motds/edit";
	}

	@PostMapping(value = "/api/profile/motd")
	public @ResponseBody String motd(@SessionAttribute(name = "config") ConfigModel config, Model model) {

		config.setShowMotd(false);
		configManager.save(config);

		// do I need to even return anything? probably yes.
		JsonObject jsonObject = Json.createObjectBuilder().build();
		return jsonObject.toString();
	}

}
