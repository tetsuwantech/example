package com.tetsuwantech.example.manager.motd;

/*-
 * #%L
 * example
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;
import com.tetsuwantech.example.database.motd.MotdEntity;
import com.tetsuwantech.example.database.motd.MotdService;
import com.tetsuwantech.example.manager.config.ConfigManager;
import com.tetsuwantech.example.web.config.ConfigModel;
import com.tetsuwantech.example.web.motd.MotdModel;

@Named(value = "MotdManager")
public class MotdManager extends GenericManager<MotdEntity, Date, MotdModel, ConfigModel> {

	// private static Logger LOG = LoggerFactory.getLogger(MotdManagerImpl.class);

	@Inject
	private MotdService motdService;

	@Inject
	private ConfigManager configManager;

	@Override
	public MotdModel createModel(MotdEntity motdEntity) {
		return new MotdModel(motdEntity);
	}

	@Override
	public void save(MotdModel motdModel) {

		// if new, create a motd, otherwise populate the new bits
		MotdEntity motdEntity;
		boolean isNew = false;
		if (motdModel.getId() == null) {
			motdEntity = new MotdEntity();

			// if this is new, then we need to set the endDate of the active motd
			List<MotdEntity> motdEntities = motdService.findAll();
			for (MotdEntity currentMotdEntity : motdEntities) {
				if (DateTimePeriodUtils.isBetween(currentMotdEntity)) {
					currentMotdEntity.setEndDate(motdModel.getStartDate());
					motdService.save(currentMotdEntity);
					break;
				}
			}

			isNew = true;
		} else {
			motdEntity = motdService.get(motdModel.getId());
		}

		// set the data that's updated
		motdEntity.setTitle(motdModel.getTitle());
		motdEntity.setDescription(motdModel.getDescription());
		motdEntity.setStartDate(motdModel.getStartDate());
		motdEntity.setEndDate(motdModel.getEndDate());

		// reset this, as it might have been a new save
		if (isNew) {
			motdService.create(motdEntity);
			motdModel.setId(motdEntity.getId());
		} else {
			motdService.save(motdEntity);
		}

		// if reset all is checked, then also update all users values
		if (motdModel.getResetAllShowMotd()) {
			configManager.resetAllShowMotd();
		}
	}

	@Override
	public List<MotdModel> findAll(ConfigModel configModel) {
		return findAll();
	}

	public List<MotdModel> findAll() {
		return super.findAll(null, null, "startDate");
	}

	@Override
	public Long countAll(SubjectModel subjectModel) {
		return super.count(null, null);
	}

	@Override
	public List<MotdModel> search(ConfigModel configModel, String text) {
		return super.search(null, null, text, new String[] { "title", "description", "startDate", "endDate" });
	}

}
